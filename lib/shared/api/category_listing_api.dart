import 'dart:math' as math;

import 'package:selby/common/common.dart';
import 'package:selby/utils/utils.dart';

abstract class CategoryListingApi extends RestfulApi {
  const CategoryListingApi();

  Future<Result<Pagination<Product>>> getCategoryListing(
    String categoryId,
    int page, {
    required int limit,
  });
}

class CategoryListingApiImpl extends CategoryListingApi {
  const CategoryListingApiImpl();

  @override
  Future<Result<Pagination<Product>>> getCategoryListing(
    String categoryId,
    int page, {
    required int limit,
  }) {
    return run(() async {
      final res = await getIt<MyHttpClient>().get(
        '/shops/$kShopId/categories',
        queryParameters: {
          'filter': '/categoryId eq "$categoryId"',
        },
      );
      return Result.success(
        Pagination.fromJson(
          res.data,
          mapper: Product.fromJson,
        ),
      );
    });
  }
}

class CategoryListingApiMock extends CategoryListingApi {
  const CategoryListingApiMock();

  @override
  Future<Result<Pagination<Product>>> getCategoryListing(
    String categoryId,
    int page, {
    required int limit,
  }) {
    final elements = cycleProducts(41);

    return Future.delayed(
      const Duration(seconds: 1),
      () => Result.success(
        Pagination<Product>(
          page: page,
          limit: limit,
          total: elements.length,
          data: elements.sublist(
            limit * (page - kDefaultPaginationStartingPage),
            math.min(elements.length,
                limit * (page - kDefaultPaginationStartingPage + 1)),
          ),
        ),
      ),
    );
  }
}
