import 'dart:math' as math;

import 'package:selby/common/common.dart';
import 'package:selby/utils/utils.dart';

abstract class CategoriesApi extends RestfulApi {
  const CategoriesApi();

  Future<Result<Pagination<Category>>> getCategories(
    int page, {
    required int limit,
  });
}

class CategoriesApiImpl extends CategoriesApi {
  const CategoriesApiImpl();

  @override
  Future<Result<Pagination<Category>>> getCategories(
    int page, {
    required int limit,
  }) {
    return run(() async {
      final res = await getIt<MyHttpClient>().get(
        '/shops/$kShopId/categories',
        queryParameters: {
          'filter': '/deletedAt eq nil',
        },
      );
      return Result.success(
        Pagination.fromJson(
          res.data,
          mapper: Category.fromJson,
        ),
      );
    });
  }
}

class CategoriesApiMock extends CategoriesApi {
  const CategoriesApiMock();

  @override
  Future<Result<Pagination<Category>>> getCategories(
    int page, {
    required int limit,
  }) {
    final elements = cycleCategories(33);

    return Future.delayed(
      const Duration(seconds: 1),
      () => Result.success(
        Pagination<Category>(
          page: page,
          limit: limit,
          total: elements.length,
          data: elements.sublist(
            limit * (page - kDefaultPaginationStartingPage),
            math.min(elements.length,
                limit * (page - kDefaultPaginationStartingPage + 1)),
          ),
        ),
      ),
    );
  }
}
