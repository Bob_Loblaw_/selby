import 'package:flutter/material.dart';
import 'package:selby/utils/utils.dart';

class HapticArea extends StatelessWidget {
  final Widget child;
  final BorderRadius borderRadius;
  final double? splashRadius;
  final VoidCallback? onTap;
  final EdgeInsets padding;
  final Color? overlayColor;

  const HapticArea({
    Key? key,
    this.borderRadius = const BorderRadius.all(
      Radius.circular(kBorderRadiusSmall),
    ),
    this.padding = const EdgeInsets.symmetric(
      horizontal: kStep,
    ),
    this.overlayColor,
    this.onTap,
    this.splashRadius,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        overlayColor: MaterialStateProperty.all(overlayColor),
        borderRadius: borderRadius,
        radius: splashRadius,
        onTap: onTap,
        child: Padding(
          padding: padding,
          child: child,
        ),
      ),
    );
  }
}
