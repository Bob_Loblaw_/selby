import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class SearchHeader extends StatelessWidget {
  const SearchHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: kStep),
        HapticArea(
          padding: EdgeInsets.zero,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(kBorderRadiusBig),
            bottomLeft: Radius.circular(kBorderRadiusBig),
            topRight: Radius.circular(kBorderRadiusSmall),
            bottomRight: Radius.circular(kBorderRadiusSmall),
          ),
          onTap: () {
            context.pop();
          },
          child: SizedBox(
            height: kTextFieldHeight,
            width: 40.0,
            child: Center(
              child: Transform.rotate(
                alignment: Alignment.center,
                angle: math.pi / 2,
                child: SvgPicture.asset(
                  SvgIcons.chevronDown,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: kStep),
        Expanded(
          child: SearchField(
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(kBorderRadiusBig),
                bottomRight: Radius.circular(kBorderRadiusBig),
                topLeft: Radius.circular(kBorderRadiusSmall),
                bottomLeft: Radius.circular(kBorderRadiusSmall),
              ),
              borderSide: BorderSide.none,
              gapPadding: 0.0,
            ),
            suffix: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                context.push(
                  FilterScreen.name,
                );
              },
              child: SizedBox.square(
                dimension: 32.0,
                child: Center(
                  child: SvgPicture.asset(
                    SvgIcons.filter,
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 2.0 * kStep),
      ],
    );
  }
}
