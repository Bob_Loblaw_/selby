import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:go_router/go_router.dart';

import 'package:selby/common/common.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

// assumes 2 in cross-axis direction
class ShopItemGridRow extends StatefulWidget {
  final Product p1;
  final Product? p2;

  const ShopItemGridRow({
    Key? key,
    required this.p1,
    this.p2,
  }) : super(key: key);

  @override
  State<ShopItemGridRow> createState() => _ShopItemGridRowState();

  static const kItemAspectRatio = 154 / 238;
}

class _ShopItemGridRowState extends State<ShopItemGridRow> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: _ShopGridItem(product: widget.p1)),
        const SizedBox(width: kStep),
        if (widget.p2 != null)
          Expanded(child: _ShopGridItem(product: widget.p2!))
        else
          const Spacer()
      ],
    );
  }
}

class _ShopGridItem extends StatelessWidget {
  final Product product;

  const _ShopGridItem({Key? key, required this.product}) : super(key: key);

  static const _kImageAspectRatio = 154 / 130;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      return SizedBox(
        width: ctrx.maxWidth,
        height: ctrx.maxWidth / ShopItemGridRow.kItemAspectRatio,
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(kBorderRadiusMedium),
            topRight: Radius.circular(kBorderRadiusMedium),
          ),
          child: ColoredBox(
            color: Theme.of(context).colorScheme.surface,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                Feedback.forTap(context);
                context.push(ProductViewScreen.name, extra: product);
              },
              child: Column(
                children: [
                  AspectRatio(
                    aspectRatio: _kImageAspectRatio,
                    child: ProductImage(product.coverId),
                  ),
                  Expanded(child: info(context)),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  Widget info(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: kStep,
        right: kStep,
        top: 2.0 * kStep,
        bottom: kStep,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: OverflownText(
              product.label,
              textAlign: TextAlign.start,
              style: TextStyles.normal,
            ),
          ),
          const SizedBox(height: kStep / 2),
          Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      product.price.toCurrencyString(
                        suffix: kRubUnicode,
                      ),
                      textAlign: TextAlign.start,
                      style: TextStyles.normalSemibold,
                    ),
                  ),
                ),
              ),
              HapticArea(
                onTap: () {},
                borderRadius: const BorderRadius.all(
                  Radius.circular(20.0),
                ),
                padding: EdgeInsets.zero,
                child: SizedBox.square(
                  dimension: 40.0,
                  child: SvgPicture.asset(
                    SvgIcons.shoppingCart,
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
