import 'package:selby/common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaginationListView<ModelClass> extends StatefulWidget {
  final void Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? cubitListener;

  final ScrollController? scrollController;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyInit;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyLoading;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptySuccess;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyFailure;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
    int index,
  ) itemBuilder;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingLoading;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingCanLoadMore;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingNoMoreItems;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingFailure;

  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;

  final EdgeInsets? padding;

  final double paginationTriggerOffsetThreshold;

  final bool autopaginate;

  const PaginationListView({
    Key? key,
    this.scrollController,
    this.cubitListener,
    this.emptyInit,
    this.emptyLoading,
    this.emptySuccess,
    this.emptyFailure,
    required this.itemBuilder,
    this.trailingLoading,
    this.trailingCanLoadMore,
    this.trailingNoMoreItems,
    this.trailingFailure,
    this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
    this.padding,
    this.paginationTriggerOffsetThreshold = 80.0,
    this.autopaginate = true,
  }) : super(key: key);

  @override
  State<PaginationListView<ModelClass>> createState() =>
      _PaginationListViewState<ModelClass>();
}

class _PaginationListViewState<ModelClass>
    extends State<PaginationListView<ModelClass>> {
  late ScrollController scrollController;
  late PaginationCubit<ModelClass> bloc;

  void setScrollController() {
    if (widget.scrollController != null) {
      scrollController = widget.scrollController!;
    } else {
      scrollController = ScrollController();
      if (widget.autopaginate) {
        scrollController.addListener(setScrollListener);
      }
    }
  }

  void setScrollListener() {
    if (scrollController.offset >
            scrollController.position.maxScrollExtent -
                widget.paginationTriggerOffsetThreshold &&
        bloc.state.canLoadMore &&
        !bloc.state.status.isLoading) {
      bloc.load();
    }
  }

  @override
  void initState() {
    assert(
      () {
        try {
          BlocProvider.of<PaginationCubit<ModelClass>>(context);
          return true;
        } catch (_) {
          return false;
        }
      }(),
      'Could not find correct BlocProvider<PaginationCubit<${ModelClass.toString()}>>. Are your types correct?',
    );
    setScrollController();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant PaginationListView<ModelClass> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.scrollController != widget.scrollController ||
        oldWidget.autopaginate != widget.autopaginate) {
      if (widget.scrollController == null) {
        scrollController.dispose();
      }
      setScrollController();
    }
  }

  @override
  void didChangeDependencies() {
    bloc = BlocProvider.of<PaginationCubit<ModelClass>>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PaginationCubit<ModelClass>,
        PaginationState<ModelClass>>(
      listener: (ctx, state) {
        widget.cubitListener?.call(ctx, state);
      },
      builder: (ctx, state) {
        if (state.items.isEmpty) {
          if (state.status.isInitial) {
            final builder = widget.emptyInit;
            if (builder != null) return builder(ctx, state);
            return _defaultEmptyInit();
          } else if (state.status.isLoading) {
            final builder = widget.emptyLoading;
            if (builder != null) return builder(ctx, state);
            return _defaultEmptyLoading();
          } else if (state.status.isFailure) {
            final builder = widget.emptyFailure;
            if (builder != null) return builder(ctx, state);
            return _defaultFailure();
          } else {
            final builder = widget.emptySuccess;
            if (builder != null) return builder(ctx, state);
            return _defaultEmptySuccess();
          }
        } else {
          return ListView.builder(
            controller: scrollController,
            keyboardDismissBehavior: widget.keyboardDismissBehavior,
            padding: widget.padding,
            itemBuilder: (ctx, index) {
              if (index == state.items.length) {
                if (state.status.isLoading) {
                  final builder = widget.trailingLoading;
                  if (builder != null) return builder(ctx, state);
                  return _defaultTrailingLoading();
                } else if (state.status.isFailure) {
                  final builder = widget.trailingFailure;
                  if (builder != null) return builder(ctx, state);
                  return _defaultFailure();
                } else {
                  if (state.canLoadMore) {
                    final builder = widget.trailingCanLoadMore;
                    if (builder != null) return builder(ctx, state);
                    return _loadMoreButton(ctx);
                  } else {
                    final builder = widget.trailingNoMoreItems;
                    if (builder != null) return builder(ctx, state);
                    return _defaultNoMoreItems();
                  }
                }
              } else {
                return widget.itemBuilder(ctx, state, index);
              }
            },
            itemCount: state.items.length + 1,
          );
        }
      },
    );
  }
}

class PaginationCustomScrollView<ModelClass> extends StatefulWidget {
  final void Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? cubitListener;

  final ScrollController? scrollController;

  final List<Widget>? leadingSlivers;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyInitSliver;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyLoadingSliver;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptySuccessSliver;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? emptyFailureSliver;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
    int index,
  ) itemBuilder;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingLoading;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingCanLoadMore;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingNoMoreItems;

  final Widget Function(
    BuildContext context,
    PaginationState<ModelClass> state,
  )? trailingFailure;

  final List<Widget>? trailingSlivers;

  final double paginationTriggerOffsetThreshold;

  final bool autopaginate;

  const PaginationCustomScrollView({
    Key? key,
    this.cubitListener,
    this.scrollController,
    this.leadingSlivers,
    this.emptyInitSliver,
    this.emptyLoadingSliver,
    this.emptySuccessSliver,
    this.emptyFailureSliver,
    required this.itemBuilder,
    this.trailingLoading,
    this.trailingCanLoadMore,
    this.trailingNoMoreItems,
    this.trailingFailure,
    this.trailingSlivers,
    this.paginationTriggerOffsetThreshold = 80.0,
    this.autopaginate = true,
  }) : super(key: key);

  @override
  State<PaginationCustomScrollView<ModelClass>> createState() =>
      _PaginationCustomScrollViewState<ModelClass>();
}

class _PaginationCustomScrollViewState<ModelClass>
    extends State<PaginationCustomScrollView<ModelClass>> {
  late ScrollController scrollController;
  late PaginationCubit<ModelClass> bloc;

  void setScrollController() {
    if (widget.scrollController != null) {
      scrollController = widget.scrollController!;
    } else {
      scrollController = ScrollController();
      if (widget.autopaginate) {
        scrollController.addListener(setScrollListener);
      }
    }
  }

  void setScrollListener() {
    if (scrollController.offset >
            scrollController.position.maxScrollExtent -
                widget.paginationTriggerOffsetThreshold &&
        bloc.state.canLoadMore &&
        !bloc.state.status.isLoading) {
      bloc.load();
    }
  }

  @override
  void initState() {
    assert(
      () {
        try {
          BlocProvider.of<PaginationCubit<ModelClass>>(context);
          return true;
        } catch (_) {
          return false;
        }
      }(),
      'Could not find correct BlocProvider<PaginationCubit<${ModelClass.toString()}>>. Are your types correct?',
    );
    setScrollController();
    super.initState();
  }

  @override
  void didUpdateWidget(
    covariant PaginationCustomScrollView<ModelClass> oldWidget,
  ) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.scrollController != widget.scrollController ||
        oldWidget.autopaginate != widget.autopaginate) {
      if (widget.scrollController == null) {
        scrollController.dispose();
      }
      setScrollController();
    }
  }

  @override
  void didChangeDependencies() {
    bloc = BlocProvider.of<PaginationCubit<ModelClass>>(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: scrollController,
      slivers: [
        if (widget.leadingSlivers != null) ...widget.leadingSlivers!,
        BlocConsumer<PaginationCubit<ModelClass>, PaginationState<ModelClass>>(
          listener: (ctx, state) {
            widget.cubitListener?.call(ctx, state);
          },
          builder: (ctx, state) {
            if (state.items.isEmpty) {
              if (state.status.isInitial) {
                final builder = widget.emptyInitSliver;
                if (builder != null) return builder(ctx, state);
                return SliverToBoxAdapter(child: _defaultEmptyInit());
              } else if (state.status.isLoading) {
                final builder = widget.emptyLoadingSliver;
                if (builder != null) return builder(ctx, state);
                return SliverToBoxAdapter(child: _defaultEmptyLoading());
              } else if (state.status.isFailure) {
                final builder = widget.emptyFailureSliver;
                if (builder != null) return builder(ctx, state);
                return SliverToBoxAdapter(child: _defaultFailure());
              } else {
                final builder = widget.emptySuccessSliver;
                if (builder != null) return builder(ctx, state);
                return SliverToBoxAdapter(child: _defaultEmptySuccess());
              }
            } else {
              return SliverList(
                delegate: SliverChildBuilderDelegate(
                  (ctx, index) {
                    if (index == state.items.length) {
                      if (state.status.isLoading) {
                        final builder = widget.trailingLoading;
                        if (builder != null) return builder(ctx, state);
                        return _defaultTrailingLoading();
                      } else if (state.status.isFailure) {
                        final builder = widget.trailingFailure;
                        if (builder != null) return builder(ctx, state);
                        return _defaultFailure();
                      } else {
                        if (state.canLoadMore) {
                          final builder = widget.trailingCanLoadMore;
                          if (builder != null) return builder(ctx, state);
                          return _loadMoreButton(ctx);
                        } else {
                          final builder = widget.trailingNoMoreItems;
                          if (builder != null) return builder(ctx, state);
                          return _defaultNoMoreItems();
                        }
                      }
                    } else {
                      return widget.itemBuilder(ctx, state, index);
                    }
                  },
                  childCount: state.items.length + 1,
                ),
              );
            }
          },
        ),
        if (widget.trailingSlivers != null) ...widget.trailingSlivers!,
      ],
    );
  }
}

Widget _defaultEmptyInit() {
  return const SizedBox();
}

Widget _defaultEmptyLoading() {
  return const Center(
    child: CircularProgressIndicator(),
  );
}

Widget _defaultTrailingLoading() {
  return const Padding(
    padding: EdgeInsets.symmetric(vertical: 8.0),
    child: Center(child: CircularProgressIndicator()),
  );
}

Widget _defaultFailure() {
  return const Padding(
    padding: EdgeInsets.all(8.0),
    child: Center(
      child: Text('failure'),
    ),
  );
}

Widget _defaultEmptySuccess() {
  return const Padding(
    padding: EdgeInsets.all(8.0),
    child: Center(
      child: Text('empty success'),
    ),
  );
}

Widget _loadMoreButton(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Center(
      child: TextButton(
        onPressed: () {
          BlocProvider.of<PaginationCubit>(context).load();
        },
        child: const Text('load more'),
      ),
    ),
  );
}

Widget _defaultNoMoreItems() {
  return const SizedBox();
}
