import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

/// We start with 0th page, but render from 1
class NumberPaginator extends StatefulWidget {
  final int totalPages;
  final int currentPage;
  final void Function(int index) onPressed;

  const NumberPaginator({
    Key? key,
    required this.totalPages,
    required this.currentPage,
    required this.onPressed,
  })  : assert(currentPage >= 0 && totalPages > currentPage),
        super(key: key);

  @override
  State<NumberPaginator> createState() => _NumberPaginatorState();
}

class _NumberPaginatorState extends State<NumberPaginator> {
  static const double idealElementDim = 48.0;

  final c = ScrollController();

  // init with true to center during first build
  bool needCentering = true;

  // expecting only ODD number of elements in row
  void centerSelection(int numElementsInRow, double elementWidth) {
    assert(numElementsInRow & 1 == 1);
    final elementsToSide = numElementsInRow >> 1;

    const minOffset = 0.0;
    final maxOffset = elementWidth * (widget.totalPages - numElementsInRow);

    final offsetFromLeftBorder =
        ((widget.currentPage - elementsToSide) * elementWidth)
            .clamp(minOffset, maxOffset);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      c.jumpTo(offsetFromLeftBorder);
    });
  }

  @override
  void didUpdateWidget(covariant NumberPaginator old) {
    if (old.currentPage != widget.currentPage ||
        old.totalPages != widget.totalPages) {
      needCentering = true;
    }

    super.didUpdateWidget(old);
  }

  @override
  void dispose() {
    c.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      final availableWidth = ctrx.maxWidth - idealElementDim * 2.0;

      int numElements = availableWidth ~/ idealElementDim;

      if (numElements >= widget.totalPages) {
        return wrapper(Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            for (int i = 0; i < widget.totalPages; i++)
              Expanded(
                child: SizedBox.expand(
                  child: element((i + 1).toString(), i),
                ),
              ),
          ],
        ));
      }

      if (numElements & 1 == 0) numElements = numElements - 1;
      final realElementWidth = availableWidth / numElements;

      if (needCentering) {
        needCentering = false;
        centerSelection(numElements, realElementWidth);
      }

      return wrapper(
        CustomScrollView(
          controller: c,
          scrollDirection: Axis.horizontal,
          slivers: [
            SliverFixedExtentList.builder(
              itemCount: widget.totalPages,
              itemExtent: realElementWidth,
              itemBuilder: (ctx, ind) {
                return SizedBox.expand(
                  child: element((ind + 1).toString(), ind),
                );
              },
            )
          ],
        ),
      );
    });
  }

  Widget wrapper(Widget fill) {
    return SizedBox(
      height: idealElementDim,
      width: double.infinity,
      child: Row(
        children: [
          arrow(false),
          Expanded(child: fill),
          arrow(true),
        ],
      ),
    );
  }

  Widget element(String text, int index) {
    final cs = Theme.of(context).colorScheme;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        HapticFeedback.selectionClick();
        widget.onPressed(index);
      },
      child: DecoratedBox(
        decoration: ShapeDecoration(
          shape: FixedLinearBorder(
            bottom: const FixedLinearBorderEdge(),
            side: BorderSide(
              color: cs.onSecondary,
              width: 1.0,
            ),
          ),
        ),
        child: Center(
          child: SizedBox(
            width: idealElementDim,
            height: double.infinity,
            child: DecoratedBox(
              decoration: ShapeDecoration(
                shape: FixedLinearBorder(
                  bottom: const FixedLinearBorderEdge(),
                  side: BorderSide(
                    color: index == widget.currentPage
                        ? cs.primary
                        : Colors.transparent,
                    width: 1.0,
                  ),
                ),
              ),
              child: Center(
                child: Text(
                  text,
                  style: TextStyles.small.copyWith(
                    color: index == widget.currentPage
                        ? cs.primary
                        : cs.onSecondary,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget arrow(bool forward) {
    final cs = Theme.of(context).colorScheme;
    final c = widget.currentPage;
    final t = widget.totalPages;
    final ar = !forward && c > 0;
    final af = forward && c < t - 1;

    return HapticArea(
      padding: EdgeInsets.zero,
      onTap: () {
        if (af) {
          widget.onPressed(widget.currentPage + 1);
        } else if (ar) {
          widget.onPressed(widget.currentPage - 1);
        }
      },
      child: SizedBox.square(
        dimension: idealElementDim,
        child: Center(
          child: Transform.rotate(
            alignment: Alignment.center,
            angle: math.pi / 2 * (forward ? -1 : 1),
            child: SvgPicture.asset(
              SvgIcons.chevronDown,
              color: af || ar ? cs.primary : cs.onSecondary,
            ),
          ),
        ),
      ),
    );
  }
}
