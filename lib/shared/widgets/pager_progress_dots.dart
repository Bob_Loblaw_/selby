import 'package:flutter/material.dart';
import 'package:selby/utils/utils.dart';

class PagerProgressDots extends StatelessWidget {
  final int size;
  final int current;

  const PagerProgressDots({
    Key? key,
    required this.size,
    required this.current,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (size == 0 || size == 1) return const SizedBox();

    return Center(
      child: SizedBox(
        height: kStep,
        width: kStep * (size * 2.0 - 1.0),
        child: Row(
          children: [
            for (int i = 0; i < size * 2 - 1; i++)
              () {
                if (i % 2 == 0) {
                  return AnimatedContainer(
                    duration: kThemeChangeDuration,
                    width: kStep,
                    height: kStep,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: (i >> 1) == current
                          ? Theme.of(context).colorScheme.onPrimary
                          : Theme.of(context).colorScheme.surface,
                    ),
                  );
                } else {
                  return const SizedBox(width: kStep);
                }
              }()
          ],
        ),
      ),
    );
  }
}
