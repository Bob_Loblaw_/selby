import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/models/models.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class CategoryHeader extends StatelessWidget {
  final Category category;

  const CategoryHeader({
    Key? key,
    required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: kScrollSheetPadding / 2),
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              category.title,
              style: TextStyles.h2,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        HapticArea(
          onTap: () {
            context.push(
              CategoryItemsScreen.name,
              extra: [category],
            );
          },
          padding: EdgeInsets.symmetric(
            horizontal: kStep,
            vertical: kStep,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Перейти',
                style: TextStyles.small.copyWith(
                  color: Theme.of(context).colorScheme.primary,
                ),
              ),
              const SizedBox(width: kStep),
              SvgPicture.asset(
                SvgIcons.tinyArrow,
                color: Theme.of(context).colorScheme.primary,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
