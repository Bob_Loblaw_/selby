import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';

import 'package:selby/common/common.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class BreadcrumbsListing extends StatelessWidget {
  final List<Category> breadcrumbs;

  BreadcrumbsListing({
    Key? key,
    required this.breadcrumbs,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 40.0,
      child: _underline(
        context,
        primary: false,
        child: Stack(
          children: [
            Positioned(
              bottom: 0.0,
              height: 1.0,
              left: 0.0,
              right: 0.0,
              child: ColoredBox(
                color: Theme.of(context).colorScheme.onSecondary,
              ),
            ),
            ListView.separated(
              padding: const EdgeInsets.symmetric(
                horizontal: kStep,
              ),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              reverse: true,
              itemBuilder: (ctx, ind) {
                return breadcrumbButton(context, ind);
              },
              separatorBuilder: (ctx, ind) {
                return separator(ctx, ind == 0);
              },
              itemCount: breadcrumbs.length + 1,
            ),
          ],
        ),
      ),
    );
  }

  Widget breadcrumbButton(BuildContext context, int ind) {
    final cs = Theme.of(context).colorScheme;
    final i = breadcrumbs.length - ind - 1;

    return _underline(
      context,
      primary: ind == 0,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 1.0),
        child: HapticArea(
          borderRadius: const BorderRadius.all(
            Radius.circular(kBorderRadiusSmall),
          ),
          overlayColor: cs.surface,
          onTap: () {
            final gr = GoRouter.of(context);
            if (i == -1) {
              gr.go(HomeScreen.name);
            } else {
              for (int j = 0; j < ind; j++) {
                gr.pop();
              }
            }
          },
          child: Center(
            child: Text(
              i == -1
                  ? breadcrumbs.isNotEmpty
                      ? 'Главная'
                      : 'Категории'
                  : breadcrumbs[i].title,
              style: TextStyles.normal.copyWith(
                color: ind == 0 ? cs.primary : cs.onSecondary,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget separator(BuildContext context, bool primaryColor) {
    final cs = Theme.of(context).colorScheme;

    return _underline(
      context,
      primary: false,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: kStep / 2,
        ),
        child: Center(
          child: Transform.translate(
            offset: const Offset(.0, 1.0),
            child: Text(
              kDotUnicode,
              style: TextStyle(
                color: primaryColor ? cs.primary : cs.onSecondary,
                fontWeight: FontWeight.w900,
                fontSize: 20,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget _underline(
  BuildContext context, {
  required bool primary,
  required Widget child,
  double strokeAlign = BorderSide.strokeAlignInside,
}) {
  final cs = Theme.of(context).colorScheme;
  return DecoratedBox(
    decoration: ShapeDecoration(
      shape: FixedLinearBorder(
        side: BorderSide(
          color: primary ? cs.primary : cs.onSecondary,
          strokeAlign: strokeAlign,
          width: 1.0,
        ),
        bottom: const FixedLinearBorderEdge(),
      ),
    ),
    child: child,
  );
}
