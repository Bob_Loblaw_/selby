import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:selby/utils/utils.dart';

class SearchField extends StatelessWidget {
  final TextEditingController? controller;
  final Widget? suffix;
  final InputBorder border;

  const SearchField({
    Key? key,
    this.controller,
    this.suffix,
    this.border = const OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(kBorderRadiusBig),
      ),
      borderSide: BorderSide.none,
      gapPadding: 0.0,
    ),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: kTextFieldHeight,
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          hintText: 'Поиск',
          hintStyle: TextStyles.small,
          filled: true,
          // todo this color is NOT in colors list because it's the same in Figma
          fillColor: const Color(0xfff3f4f5),
          contentPadding: const EdgeInsets.symmetric(
            vertical: 4.0,
            horizontal: 8.0,
          ),
          focusedErrorBorder: border,
          disabledBorder: border,
          enabledBorder: border,
          focusedBorder: border,
          errorBorder: border,
          border: border,
          isDense: true,
          prefixIcon: SvgPicture.asset(
            SvgIcons.search,
          ),
          suffixIcon: suffix,
        ),
      ),
    );
  }
}
