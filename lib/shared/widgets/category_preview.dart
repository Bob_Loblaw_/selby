import 'package:flutter/material.dart';

import 'package:selby/common/models/models.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

enum CategoryPreviewDisplayType { grid, crossAxisPager, mainAxisList }

class CategoryPreview extends StatelessWidget {
  final CategoryPreviewDisplayType type;
  final List<Product> products;
  final bool slivers;

  const CategoryPreview({
    Key? key,
    required this.type,
    required this.products,
    required this.slivers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (products.isEmpty) return const SizedBox();

    return switch (type) {
      CategoryPreviewDisplayType.grid => grid(),
      CategoryPreviewDisplayType.crossAxisPager => pager(),
      CategoryPreviewDisplayType.mainAxisList => list(),
    };
  }

  Widget grid() {
    if (slivers) {
      return SliverList(
        delegate: SliverChildBuilderDelegate(
          (ctx, i) {
            if (i & 1 == 1) {
              return const SizedBox(height: kStep);
            } else {
              final p1 = products[i];
              final p2 = products.length == i + 1 ? null : products[i + 1];
              return ShopItemGridRow(p1: p1, p2: p2);
            }
          },
          childCount: (products.length / 2.0).ceil() * 2 - 1,
        ),
      );
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          for (int i = 0; i < (products.length / 2.0).ceil() * 2 - 1; i++)
            () {
              if (i & 1 == 1) {
                return const SizedBox(height: kStep);
              } else {
                final p1 = products[i];
                final p2 = products.length == i + 1 ? null : products[i + 1];
                return ShopItemGridRow(p1: p1, p2: p2);
              }
            }()
        ],
      );
    }
  }

  Widget pager() {
    final pager = _CategoryProductListPager(children: products);

    if (slivers) {
      return SliverToBoxAdapter(child: pager);
    } else {
      return pager;
    }
  }

  Widget list() {
    if (slivers) {
      return SliverList(
        delegate: SliverChildBuilderDelegate(
          (ctx, ind) {
            return Padding(
              padding: const EdgeInsets.only(bottom: kStep),
              child: ShopListItem(product: products[ind]),
            );
          },
          childCount: products.length,
        ),
      );
    } else {
      // category previews are limited-size items in main axis direction
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: products.map((e) {
          return Padding(
            padding: const EdgeInsets.only(bottom: kStep),
            child: ShopListItem(product: e),
          );
        }).toList(),
      );
    }
  }
}

class _CategoryProductListPager extends StatefulWidget {
  final List<Product> children;

  const _CategoryProductListPager({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  State<_CategoryProductListPager> createState() =>
      _CategoryProductListPagerState();
}

class _CategoryProductListPagerState extends State<_CategoryProductListPager> {
  PageController? c;
  final current = ValueNotifier<int>(0);

  int get itemCount => (widget.children.length / 2.0).ceil().clamp(0, 7);

  @override
  void dispose() {
    c?.dispose();
    current.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      if (c == null) {
        c = PageController(
          initialPage: 0,
          viewportFraction:
              1.0 - ((kScrollSheetPadding * 2.0 - kStep) / ctrx.maxWidth),
        );
        c!.addListener(() {
          if (c?.page == null) return;
          final page = c!.page!.round();
          if (current.value != page) {
            current.value = page;
          }
        });
      }

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          LayoutBuilder(builder: (ctx, ctrx) {
            return SizedBox(
              width: ctrx.maxWidth,
              height: ((ctrx.maxWidth - 5.0 * kStep) / 2) /
                  ShopItemGridRow.kItemAspectRatio,
              child: pageView(),
            );
          }),
          const SizedBox(height: kStep),
          ListenableBuilder(
            listenable: current,
            builder: (ctx, _) {
              return PagerProgressDots(
                size: itemCount,
                current: current.value,
              );
            },
          ),
        ],
      );
    });
  }

  Widget pageView() {
    return PageView.builder(
      controller: c,
      itemBuilder: (ctx, ind) {
        final i1 = ind << 1;
        final i2 = i1 + 1;
        final p1 = widget.children[i1];
        final p2 = widget.children.length <= i2 ? null : widget.children[i2];

        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: kStep / 2.0,
          ),
          child: ShopItemGridRow(p1: p1, p2: p2),
        );
      },
      itemCount: itemCount,
    );
  }
}
