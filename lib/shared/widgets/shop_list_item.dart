import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/models/models.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class ShopListItem extends StatelessWidget {
  final Product product;

  const ShopListItem({
    Key? key,
    required this.product,
  }) : super(key: key);

  static const double aspectRatio = 227 / 97;

  @override
  Widget build(BuildContext context) {
    final cs = Theme.of(context).colorScheme;

    return LayoutBuilder(builder: (ctx, ctrx) {
      final w = ctrx.maxWidth;
      final h = w / aspectRatio;

      return ClipRRect(
        borderRadius: const BorderRadiusDirectional.only(
          bottomStart: Radius.circular(kBorderRadiusMedium),
          topStart: Radius.circular(kBorderRadiusMedium),
        ),
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Feedback.forTap(context);
            context.push(
              ProductViewScreen.name,
              extra: product,
            );
          },
          child: SizedBox(
            width: w,
            height: h,
            child: ColoredBox(
              color: cs.surface,
              child: Row(
                children: [
                  SizedBox.square(
                    dimension: h,
                    child: ProductImage(
                      product.coverId,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: kStep,
                        left: 2.0 * kStep,
                        right: kStep,
                        bottom: kStep / 2,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: OverflownText(
                              product.label,
                              textAlign: TextAlign.start,
                              style: TextStyles.normal,
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    product.price.toCurrencyString(
                                      suffix: kRubUnicode,
                                    ),
                                    textAlign: TextAlign.start,
                                    style: TextStyles.normalSemibold,
                                  ),
                                ),
                              ),
                              HapticArea(
                                onTap: () {},
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(20.0),
                                ),
                                padding: EdgeInsets.zero,
                                child: SizedBox.square(
                                  dimension: 40.0,
                                  child: SvgPicture.asset(
                                    SvgIcons.shoppingCart,
                                    color: cs.primary,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
