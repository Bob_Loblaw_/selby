import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:selby/utils/utils.dart';

enum ScaleBaseDimension { min, max }

class ProductImage extends StatelessWidget {
  final String? url;
  final BoxFit fit;
  final ScaleBaseDimension placeholderScaleBaseDimension;

  const ProductImage(
    this.url, {
    Key? key,
    this.fit = BoxFit.cover,
    this.placeholderScaleBaseDimension = ScaleBaseDimension.min,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final url = this.url;
    if (url == null) {
      return _NoImagePlaceholder(
        baseDimension: placeholderScaleBaseDimension,
      );
    }

    return Image.asset(
      url,
      fit: fit,
      gaplessPlayback: true,
    );
  }
}

class _NoImagePlaceholder extends StatelessWidget {
  final ScaleBaseDimension baseDimension;

  const _NoImagePlaceholder({
    Key? key,
    required this.baseDimension,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      final dim = baseDimension == ScaleBaseDimension.min
          ? math.min(ctrx.maxWidth, ctrx.maxHeight) / 3.0
          : math.max(ctrx.maxWidth, ctrx.maxHeight) / 3.0;

      return ColoredBox(
        color: const Color.fromARGB(255, 251, 251, 251), //todo?
        child: Center(
          child: SizedBox.square(
            dimension: dim,
            child: Image.asset(Images.noImagePlaceholder),
          ),
        ),
      );
    });
  }
}
