import 'package:flutter/material.dart';

class OverflownText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;

  const OverflownText(
    this.text, {
    Key? key,
    this.style,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      final max = getMaxLinesWithoutOverflow(
        text,
        ctx,
        ctrx,
        style: style,
      );

      if (max == 0) {
        // handle case where not a single line can fit.
        TextStyle? style = this.style ?? DefaultTextStyle.of(ctx).style;

        while (true) {
          final newStyle = style!.copyWith(
            fontSize: style.fontSize! - 1,
          );

          final max = getMaxLinesWithoutOverflow(
            text,
            ctx,
            ctrx,
            style: newStyle,
          );

          if (max == null) {
            return Text(
              text,
              style: newStyle,
              textAlign: textAlign,
            );
          }

          if (max > 0) {
            return Text(
              text,
              maxLines: max,
              style: newStyle,
              textAlign: textAlign,
              overflow: TextOverflow.ellipsis,
            );
          }

          style = newStyle;
        }
      }

      return Text(
        text,
        maxLines: max,
        style: style,
        textAlign: textAlign,
        overflow: max == null ? null : TextOverflow.ellipsis,
      );
    });
  }
}

// returns null if no clipping is needed
int? getMaxLinesWithoutOverflow(
  String text,
  BuildContext context,
  BoxConstraints ctrx, {
  TextStyle? style,
}) {
  if (ctrx.maxHeight.isInfinite) return null;

  final textDir = Directionality.of(context);
  final tsf = MediaQuery.textScaleFactorOf(context);

  final span = TextSpan(text: text, style: style);
  final tp1 = TextPainter(
    text: span,
    textDirection: textDir,
    textScaleFactor: tsf,
  );

  tp1.layout(maxWidth: ctrx.maxWidth);

  if (ctrx.maxHeight > tp1.height) return null;

  final metrics = tp1.computeLineMetrics();
  double total = 0;

  for (int i = 0; i < metrics.length; i++) {
    if (total + metrics[i].height > ctrx.maxHeight) {
      return i;
    } else {
      total += metrics[i].height;
    }
  }

  return null;
}
