import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SwitchAppearance {
  final Size trackSize;
  final Widget Function(BuildContext ctx, double anim)? trackBuilder;
  final Widget Function(BuildContext ctx, double anim)? thumbBuilder;

  SwitchAppearance({
    this.trackSize = _appSwitchDefaultTrackSize,
    this.thumbBuilder,
    this.trackBuilder,
  });

  factory SwitchAppearance.painted({
    Size trackSize = _appSwitchDefaultTrackSize,
  }) {
    return SwitchAppearance(
      trackSize: trackSize,
      trackBuilder: (ctx, anim) {
        return DefaultTrackPainter(size: trackSize, anim: anim);
      },
      thumbBuilder: (ctx, anim) {
        return DefaultThumbPainter(size: trackSize, anim: anim);
      },
    );
  }
}

/// A custom switch implementation, because Flutter's own is unfitting.
///
/// Thumb size is [trackSize.height].
class CustomSwitch extends StatefulWidget {
  final bool isActive;
  final void Function(bool active) onToggled;
  final SwitchAppearance? appearance;

  CustomSwitch({
    Key? key,
    required this.isActive,
    required this.onToggled,
    this.appearance,
  })  : assert(() {
          if (appearance == null) return true;
          return appearance.trackSize.width > appearance.trackSize.height;
        }()),
        super(key: key);

  @override
  State<CustomSwitch> createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  late final controller = AnimationController(
    vsync: this,
    duration: const Duration(
      milliseconds: 250,
    ),
    value: widget.isActive ? 1.0 : 0.0,
  );

  Size get size => widget.appearance?.trackSize ?? _appSwitchDefaultTrackSize;

  @override
  void didUpdateWidget(covariant CustomSwitch oldWidget) {
    if (widget.isActive &&
        (controller.status == AnimationStatus.dismissed ||
            controller.status == AnimationStatus.reverse)) {
      controller.forward();
    } else if (!widget.isActive &&
        (controller.status == AnimationStatus.completed ||
            controller.status == AnimationStatus.forward)) {
      controller.reverse();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);

        if (controller.isAnimating) {
          controller.stop();
        }

        if (!widget.isActive) {
          controller.forward();
          widget.onToggled(true);
        } else {
          controller.reverse();
          widget.onToggled(false);
        }
      },
      behavior: HitTestBehavior.translucent,
      child: SizedBox.fromSize(
        size: size,
        child: AnimatedBuilder(
          animation: controller,
          builder: (context, anim) {
            return Stack(
              children: [
                Positioned.fill(child: track(context)),
                Positioned(
                  top: 0.0,
                  bottom: 0.0,
                  width: size.height,
                  left: getThumbLeftOffset(),
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onHorizontalDragStart: (det) {
                      controller.stop();
                    },
                    onHorizontalDragUpdate: (det) {
                      final dx = det.primaryDelta!;
                      final dPercent = dx / (size.width - size.height);
                      controller.value = controller.value + dPercent;
                    },
                    onHorizontalDragEnd: (det) {
                      if (controller.value > .5) {
                        controller.animateTo(1.0);
                        widget.onToggled(true);
                      } else {
                        controller.animateTo(0.0);
                        widget.onToggled(false);
                      }
                    },
                    child: thumb(context),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  Widget track(BuildContext context) {
    if (widget.appearance?.trackBuilder != null) {
      return widget.appearance!.trackBuilder!(context, controller.value);
    } else {
      return _defaultTrack();
    }
  }

  Widget thumb(BuildContext context) {
    if (widget.appearance?.thumbBuilder != null) {
      return widget.appearance!.thumbBuilder!(context, controller.value);
    } else {
      return _defaultThumb();
    }
  }

  Widget _defaultTrack() {
    return Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(size.height / 2),
          ),
          side: const BorderSide(
            width: 1.5,
            color: Colors.grey,
            strokeAlign: 2.0,
          ),
        ),
      ),
    );
  }

  Widget _defaultThumb() {
    return const Material(
      color: Colors.white,
      elevation: 3,
      shape: CircleBorder(),
    );
  }

  double getThumbLeftOffset() {
    return (size.width - size.height) * controller.value;
  }
}

const Size _appSwitchDefaultTrackSize = Size(52.0, 24.0);

class DefaultTrackPainter extends StatelessWidget {
  final Size size;
  final double anim;

  const DefaultTrackPainter({
    Key? key,
    required this.size,
    required this.anim,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _TrackPainter(
        size: size,
        cs: Theme.of(context).colorScheme,
        anim: anim,
      ),
    );
  }
}

class DefaultThumbPainter extends StatelessWidget {
  final Size size;
  final double anim;

  const DefaultThumbPainter({
    Key? key,
    required this.size,
    required this.anim,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _ThumbPainter(
        size: size,
        cs: Theme.of(context).colorScheme,
        anim: anim,
      ),
    );
  }
}

class _TrackPainter extends CustomPainter {
  final Size size;
  final ColorScheme cs;
  final double anim;

  _TrackPainter({
    required this.size,
    required this.cs,
    required this.anim,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final p = Paint()..color = Color.lerp(cs.onSecondary, cs.primary, anim)!;
    canvas.drawRRect(
      RRect.fromRectAndRadius(
        Offset.zero & size,
        Radius.circular(size.shortestSide / 2),
      ),
      p,
    );
  }

  @override
  bool shouldRepaint(covariant _TrackPainter old) {
    return old.cs != cs || old.anim != anim;
  }
}

class _ThumbPainter extends CustomPainter {
  final Size size;
  final ColorScheme cs;
  final double anim;

  _ThumbPainter({
    required this.size,
    required this.cs,
    required this.anim,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final p = Paint()..color = cs.background;
    final shortest = size.shortestSide;
    final endOffset = Offset(size.width - shortest, 0);
    final animOffset = Offset.lerp(Offset.zero, endOffset, anim)!;
    const shrink = 2.0;
    canvas.drawOval((animOffset & size).deflate(shrink), p);
  }

  @override
  bool shouldRepaint(covariant _ThumbPainter old) {
    return old.cs != cs || old.anim != anim;
  }
}
