import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

part 'widgets/bottom_nav_bar.dart';

class MainScreenShell extends StatelessWidget {
  final StatefulNavigationShell shell;
  final List<Widget> children;

  const MainScreenShell({
    Key? key,
    required this.shell,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: KeyboardVisibilityBuilder(
        builder: (ctx, vis) {
          if (vis) {
            return content();
          } else {
            return noKeyboard(ctx);
          }
        },
      ),
    );
  }

  Widget noKeyboard(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 0.0,
          left: 0.0,
          right: 0.0,
          bottom: kTabBarHeight + MediaQuery.of(context).padding.bottom,
          child: MediaQuery.removePadding(
            context: context,
            removeBottom: true,
            child: content(),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: _SelbyBottomNavBar(shell: shell),
        ),
      ],
    );
  }

  Widget content() {
    return IndexedStack(
      index: shell.currentIndex,
      children: children.mapIndexed((i, e) {
        return AnimatedOpacity(
          opacity: i == shell.currentIndex ? 1.0 : 0.0,
          duration: kThemeChangeDuration,
          child: e,
        );
      }).toList(),
    );
  }
}
