import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/common.dart';
import 'package:selby/screens/main/main.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

part 'widgets/category_pager.dart';
part 'widgets/category_circle.dart';
part 'widgets/home_screen_header_sliver.dart';

const kScrollSheetPadding = 2.0 * kStep;

class HomeScreen extends StatefulWidget {
  static const String name = '/home';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  final gk = GlobalKey();
  double? height;

  @override
  void didChangeTextScaleFactor() {
    measure();
    super.didChangeTextScaleFactor();
  }

  void measure() {
    height = null;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        height = gk.currentContext?.size?.height;
      });
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    measure();
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (height == null) {
      return Offstage(
        offstage: true,
        child: Center(
          child: _InfoBlock(
            key: gk,
          ),
        ),
      );
    }

    return CustomScrollView(
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      slivers: [
        SliverLayoutBuilder(
          builder: (ctx, ctrx) {
            return SliverPersistentHeader(
              delegate: _CustomSliverDelegate(
                availableSpace: ctrx.crossAxisExtent,
                topInsets: MediaQuery.of(context).padding.top,
                infoBlockDimension: height!,
              ),
              floating: false,
              pinned: true,
            );
          },
        ),
        BlocProvider(
          create: (ctx) => MainScreenCategoriesCubit(7)..load(),
          child: BlocConsumer<MainScreenCategoriesCubit, SingleActionState>(
            listener: (ctx, state) {},
            builder: (ctx, state) {
              if (state.isInitial || state.isLoading) {
                return const SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else if (state.isFailure) {
                return const SliverFillRemaining(
                  child: Center(
                    child: Text('Oshibka'),
                  ),
                );
              } else {
                state as SASuccess<List<Category>>;
                return withContent(context, state);
              }
            },
          ),
        ),
      ],
    );
  }

  Widget withContent(BuildContext context, SASuccess<List<Category>> state) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        childCount: state.result.length,
        (ctx, ind) {
          if (ind == 0) {
            return CategoryPager(categories: state.result);
          }

          final i = ind - 1;
          final cat = state.result[i];

          if (i & 1 == 0) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: kScrollSheetPadding / 2,
                vertical: kStep * 2.0,
              ),
              child: CategoryHeader(category: cat),
            );
          } else {
            return Padding(
              padding: (i >> 1) % 3 == 1
                  ? EdgeInsets.zero
                  : const EdgeInsets.symmetric(
                      horizontal: kScrollSheetPadding,
                    ),
              child: CategoryPreview(
                slivers: false,
                products: cycleProducts(5),
                type: CategoryPreviewDisplayType.values[(i >> 1) % 3],
              ),
            );
          }
        },
      ),
    );
  }
}
