part of '../home_screen.dart';

class CategoryPager extends StatefulWidget {
  final List<Category> categories;

  const CategoryPager({
    Key? key,
    required this.categories,
  }) : super(key: key);

  @override
  State<CategoryPager> createState() => _CategoryPagerState();
}

class _CategoryPagerState extends State<CategoryPager>
    with WidgetsBindingObserver {
  PageController? c;
  final current = ValueNotifier<int>(0);
  final gk = GlobalKey();
  double? height;

  void measure() {
    height = null;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        height = gk.currentContext?.size?.height;
      });
    });
  }

  @override
  void didChangeMetrics() {
    measure();
    super.didChangeMetrics();
  }

  @override
  void didChangeTextScaleFactor() {
    measure();
    super.didChangeTextScaleFactor();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    measure();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant CategoryPager oldWidget) {
    measure();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void setState(VoidCallback fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void dispose() {
    c?.dispose();
    current.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.categories.isEmpty) return const SizedBox();

    if (height == null) {
      return Offstage(
        offstage: true,
        child: categoryRow(
          widget.categories[0].copyWith(title: '\n'),
          null,
          null,
          key: gk,
        ),
      );
    }

    return LayoutBuilder(builder: (ctx, ctrx) {
      final itemCount = (widget.categories.length / 3.0).ceil().clamp(0, 7);

      if (c == null) {
        c = PageController(
          initialPage: 0,
          viewportFraction:
              1.0 - ((kScrollSheetPadding * 2.0 - kStep) / ctrx.maxWidth),
        );
        c!.addListener(() {
          if (c?.page == null) return;
          final page = c!.page!.round();
          if (current.value != page) {
            current.value = page;
          }
        });
      }

      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: double.infinity,
            height: height,
            child: PageView.builder(
              controller: c,
              itemBuilder: (ctx, ind) {
                ind = 3 * ind;
                final c = widget.categories;
                final c1 = c[ind];
                final c2 = ind + 1 >= c.length ? null : c[ind + 1];
                final c3 = ind + 2 >= c.length ? null : c[ind + 2];
                return categoryRow(c1, c2, c3);
              },
              itemCount: itemCount,
            ),
          ),
          const SizedBox(height: kStep),
          ListenableBuilder(
            listenable: current,
            builder: (ctx, _) {
              return PagerProgressDots(
                size: itemCount,
                current: current.value,
              );
            },
          ),
        ],
      );
    });
  }

  Widget categoryRow(Category c1, Category? c2, Category? c3, {Key? key}) {
    return Padding(
      key: key,
      padding: const EdgeInsets.symmetric(
        horizontal: kStep / 2.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: CategoryCircle(
              category: c1,
            ),
          ),
          const SizedBox(width: kStep),
          if (c2 != null)
            Expanded(
              child: CategoryCircle(
                category: c2,
              ),
            )
          else
            const Expanded(
              child: ViewAllCategoriesCircle(),
            ),
          const SizedBox(width: kStep),
          if (c3 != null)
            Expanded(
              child: CategoryCircle(
                category: c3,
              ),
            )
          else if (c2 != null)
            const Expanded(
              child: ViewAllCategoriesCircle(),
            )
          else
            const Spacer(),
        ],
      ),
    );
  }
}
