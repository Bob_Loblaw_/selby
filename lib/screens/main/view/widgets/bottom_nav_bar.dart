part of '../shell.dart';

class _SelbyBottomNavBar extends StatelessWidget {
  final StatefulNavigationShell shell;

  const _SelbyBottomNavBar({
    Key? key,
    required this.shell,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: ClipRect(
        child: SizedBox(
          width: double.infinity,
          height: kTabBarHeight,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: kStep,
              horizontal: 2.0 * kStep,
            ),
            child: Row(
              children: [
                const Spacer(flex: 5),
                Expanded(
                  flex: 10,
                  child: bottomBarElement(
                    context,
                    svgIconPath: SvgIcons.home,
                    index: 0,
                  ),
                ),
                Expanded(
                  flex: 10,
                  child: bottomBarElement(
                    context,
                    svgIconPath: SvgIcons.shoppingBag,
                    index: 1,
                  ),
                ),
                Expanded(
                  flex: 10,
                  child: bottomBarElement(
                    context,
                    svgIconPath: SvgIcons.profile,
                    index: 2,
                  ),
                ),
                const Spacer(flex: 5),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget bottomBarElement(
    BuildContext context, {
    required String svgIconPath,
    required int index,
  }) {
    final cs = Theme.of(context).colorScheme;
    const dim = kTabBarHeight * 1.5;

    return OverflowBox(
      maxHeight: kTabBarHeight * 1.5,
      maxWidth: kTabBarHeight * 1.5,
      child: HapticArea(
        padding: EdgeInsets.zero,
        borderRadius: const BorderRadius.all(
          Radius.circular(dim / 2),
        ),
        splashRadius: dim / 2,
        onTap: () {
          shell.goBranch(index);
        },
        child: SizedBox.square(
          dimension: dim,
          child: Center(
            child: SvgPicture.asset(
              svgIconPath,
              color: index == shell.currentIndex ? cs.primary : cs.onBackground,
            ),
          ),
        ),
      ),
    );
  }
}
