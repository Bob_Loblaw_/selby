part of '../home_screen.dart';

class CategoryCircle extends StatelessWidget {
  final Category category;

  const CategoryCircle({
    Key? key,
    required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Feedback.forTap(context);
          context.push(
            CategoriesScreen.name,
            extra: [category],
          );
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox.square(
              dimension: ctrx.maxWidth,
              child: ClipOval(
                child: ProductImage(
                  category.avatarId,
                  key: ValueKey(category.avatarId),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(height: kStep / 2.0),
            Text(
              category.title,
              maxLines: 2,
              style: TextStyles.small,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      );
    });
  }
}

class ViewAllCategoriesCircle extends StatelessWidget {
  const ViewAllCategoriesCircle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          Feedback.forTap(context);
          context.push(
            CategoriesScreen.name,
            extra: [],
          );
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox.square(
              dimension: ctrx.maxWidth,
              child: const ClipOval(
                child: ProductImage(null),
              ),
            ),
            const SizedBox(height: kStep / 2.0),
            const Text(
              'Посмотреть все',
              maxLines: 2,
              style: TextStyles.small,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      );
    });
  }
}
