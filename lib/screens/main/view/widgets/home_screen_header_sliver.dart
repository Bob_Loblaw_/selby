part of '../home_screen.dart';

class _CustomSliverDelegate extends SliverPersistentHeaderDelegate {
  final double availableSpace;
  final double topInsets;
  final double infoBlockDimension;

  static bool _isSystemBarDark = false;

  static const String _imageKeyValue = '_image';
  static const String _infoBlockKeyValue = '_infoBlock';
  static const String _searchBarKeyValue = '_searchBar';
  static const String _fadeAreaKeyValue = '_fadeAreaKey';

  _CustomSliverDelegate({
    required this.availableSpace,
    required this.topInsets,
    required this.infoBlockDimension,
  });

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    final delta = maxExtent - minExtent;
    final progress = math.max((delta - shrinkOffset), 0.0);
    final additionalSpace = math.max(0.0, topInsets - progress);
    final opacity = 1.0 - (shrinkOffset * 3.0 / delta).clamp(0.0, 1.0);
    final h = kScrollSheetPadding + kTextFieldHeight + additionalSpace;

    if (_isSystemBarDark && additionalSpace * 2.0 > topInsets + 16.0) {
      setBrightOverlayStyle();
      _isSystemBarDark = false;
    } else if (!_isSystemBarDark && additionalSpace * 2.0 < topInsets + 16.0) {
      setDarkOverlayStyle();
      _isSystemBarDark = true;
    }

    final theme = Theme.of(context);
    final cs = theme.colorScheme;

    return Stack(
      children: [
        // background image
        Positioned(
          key: const ValueKey(_imageKeyValue),
          left: 0.0,
          right: 0.0,
          top: 0.0,
          bottom: 24.0,
          child: ClipRect(
            child: Transform.scale(
              scale: 1.0 +
                  Curves.easeOut.transform(
                          (shrinkOffset / delta).clamp(0.0, .5) * 2.0) /
                      2.0,
              alignment: Alignment.center,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: cs.background.withOpacity(
                    opacity / 3.0,
                  ),
                ),
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.matrix([
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        (1.25 - (shrinkOffset / delta)).clamp(0.0, 1.0),
                        0,
                      ]),
                      alignment: Alignment.center,
                      image: Image.asset(Images.shopCover).image,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        // info about shop (title and description)
        if (opacity > 0.0)
          Positioned(
            key: const ValueKey(_infoBlockKeyValue),
            left: 0.0,
            right: 0.0,
            bottom: h + 24.0 + 8.0, // 8-pixel gap
            height: infoBlockDimension,
            child: Opacity(
              opacity: opacity,
              child: const _InfoBlock(),
            ),
          ),
        // search bar that remains constant
        Positioned(
          key: const ValueKey(_searchBarKeyValue),
          left: 0.0,
          right: 0.0,
          bottom: 24.0,
          height: h,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(kBorderRadiusBig),
                topRight: Radius.circular(kBorderRadiusBig),
              ),
              color: Colors.white,
            ),
            child: Padding(
              padding: EdgeInsets.only(
                top: kScrollSheetPadding + additionalSpace,
                left: kScrollSheetPadding,
                right: kScrollSheetPadding,
              ),
              child: const SearchField(),
            ),
          ),
        ),
        // fade-out for sliver-list elements
        Positioned(
          key: const ValueKey(_fadeAreaKeyValue),
          bottom: 0.0,
          height: 25.0, // plus 1 safety pixel to fix clipping picture
          right: 0.0,
          left: 0.0,
          child: DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  cs.background,
                  cs.background,
                  cs.background.withAlpha(0),
                ],
                stops: const [.0, .2, .8],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => availableSpace + 24.0;

  @override
  double get minExtent =>
      topInsets + kScrollSheetPadding + kTextFieldHeight + 16.0;

  @override
  bool shouldRebuild(covariant _CustomSliverDelegate old) {
    return old.topInsets != topInsets ||
        old.infoBlockDimension != infoBlockDimension ||
        old.availableSpace != availableSpace;
  }
}

class _InfoBlock extends StatelessWidget {
  const _InfoBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: kScrollSheetPadding,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 40.0,
            child: Row(
              children: [
                SvgPicture.asset(SvgIcons.shopLogo),
                const SizedBox(width: kStep),
                Text(
                  'Nedera',
                  style: TextStyles.h2.copyWith(
                    color: Theme.of(context).colorScheme.background,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: kStep),
          Text(
            'Мебельный магазин Nedera - это место, где вы найдёте все, что необходимо для создания уютного и стильного интерьера.',
            style: TextStyles.small.copyWith(
              color: Theme.of(context).colorScheme.background,
            ),
          ),
        ],
      ),
    );
  }
}
