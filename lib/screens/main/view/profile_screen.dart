import 'package:flutter/material.dart';
import 'package:selby/utils/utils.dart';

class ProfileScreen extends StatelessWidget {
  static const String name = '/profile';
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          const _Header(),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 2 * kStep,
            ),
            child: Column(
              children: [
                _Button('Мои реквизиты', onTap: () {}),
                _Button('Приватность', onTap: () {}),
                _Button('Выйти из аккаунта', onTap: () {}),
                _Button('Удалить личный кабинет', onTap: () {}),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: ShapeDecoration(
        shape: FixedLinearBorder(
          bottom: const FixedLinearBorderEdge(),
          side: BorderSide(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
      ),
      child: const Padding(
        padding: EdgeInsets.symmetric(
          vertical: 2 * kStep,
          horizontal: 3 * kStep,
        ),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Мои данные',
            style: TextStyles.h2,
          ),
        ),
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String text;
  final VoidCallback onTap;

  const _Button(
    this.text, {
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: ShapeDecoration(
        shape: FixedLinearBorder(
          bottom: const FixedLinearBorderEdge(),
          side: BorderSide(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 2 * kStep,
        ),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            text,
            style: TextStyles.normal,
          ),
        ),
      ),
    );
  }
}
