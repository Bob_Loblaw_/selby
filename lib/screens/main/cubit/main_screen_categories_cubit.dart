import 'package:selby/common/common.dart';
import 'package:selby/shared/api/api.dart';
import 'package:selby/utils/utils.dart';

class MainScreenCategoriesCubit extends SingleActionCubitNoArg<List<Category>> {
  final int limit;

  MainScreenCategoriesCubit(this.limit);

  @override
  Future<Result<List<Category>>> action() async {
    final res = await getIt<CategoriesApi>().getCategories(
      kDefaultPaginationStartingPage,
      limit: limit,
    );

    if (res.success) {
      return Result.success(res.data.data);
    } else {
      return Result.failure(res.error);
    }
  }
}
