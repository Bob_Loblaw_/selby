import 'package:selby/common/common.dart';
import 'package:selby/shared/api/api.dart';
import 'package:selby/utils/utils.dart';

class MainScreenCategoryListingCubit
    extends SingleActionCubitWithArg<Category, List<Product>> {
  final String categoryId;

  final int limit;

  MainScreenCategoryListingCubit(this.categoryId, {required this.limit});

  @override
  Future<Result<List<Product>>> action(Category arg) async {
    final res = await getIt<CategoryListingApi>().getCategoryListing(
      categoryId,
      0,
      limit: limit,
    );

    if (res.success) {
      return Result.success(res.data.data);
    } else {
      return Result.failure(res.error);
    }
  }
}
