import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/common.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class CategoriesScreen extends StatefulWidget {
  final List<Category> breadcrumbs;

  const CategoriesScreen({
    Key? key,
    required this.breadcrumbs,
  }) : super(key: key);

  static const String name = '/categories';

  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  final sc = ScrollController();

  @override
  void dispose() {
    sc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CategoriesSinglePageLoaderCubit>(
      create: (_) => CategoriesSinglePageLoaderCubit(15)
        ..loadPage(kDefaultPaginationStartingPage),
      child: ColoredBox(
        color: Theme.of(context).colorScheme.background,
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(height: 2.0 * kStep),
              const SearchHeader(),
              const SizedBox(height: 2.0 * kStep),
              BreadcrumbsListing(breadcrumbs: widget.breadcrumbs),
              Expanded(child: pagination()),
            ],
          ),
        ),
      ),
    );
  }

  Widget pagination() {
    return BlocConsumer<CategoriesSinglePageLoaderCubit,
        PaginationState<Category>>(
      listener: (ctx, state) {
        if (state.status.isSuccess && state.items.length <= state.limit) {
          // if we loaded a page with a paginator widget
          WidgetsBinding.instance.addPostFrameCallback((_) {
            // give it time to settle in
            sc.animateTo(
              0,
              duration: const Duration(milliseconds: 750),
              curve: Curves.easeOutExpo,
            );
          });
        }
      },
      builder: (ctx, state) {
        return Stack(
          children: [
            Positioned.fill(child: list(ctx, state)),
            Positioned.fill(
              child: AnimatedOpacity(
                duration: kThemeChangeDuration,
                opacity: state.status.isLoading ? 1.0 : .0,
                child: IgnorePointer(
                  key: ValueKey(state.status.index),
                  ignoring: !state.status.isLoading,
                  child: ColoredBox(
                    color: Theme.of(context)
                        .colorScheme
                        .background
                        .withOpacity(.5),
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget list(BuildContext ctx, PaginationState<Category> state) {
    final l = state.items.length;

    return ListView.builder(
      controller: sc,
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      padding: const EdgeInsets.only(
        bottom: 3.0 * kStep,
        top: 2.0 * kStep,
        left: 2.0 * kStep,
        right: 2.0 * kStep,
      ),
      itemCount: l + 1,
      itemBuilder: (ctx, ind) {
        if (ind == l) {
          return paginationControl(ctx, state);
        } else {
          return switch (ind % 3) {
            0 => Padding(
                padding: const EdgeInsets.only(bottom: 3.0 * kStep),
                child: row2(
                  state.items[ind],
                  (ind + 1 == l) ? null : state.items[ind + 1],
                ),
              ),
            1 => const SizedBox(),
            2 => Padding(
                padding: const EdgeInsets.only(bottom: 3.0 * kStep),
                child: row1(state.items[ind]),
              ),
            _ => throw UnsupportedError('unreachable'),
          };
        }
      },
    );
  }

  Widget paginationControl(BuildContext ctx, PaginationState<Category> state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Visibility(
          visible: state.canLoadMore,
          child: SizedBox(
            width: double.infinity,
            child: FilledButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Theme.of(context).colorScheme.secondary,
                ),
                textStyle: MaterialStateProperty.all(
                  TextStyles.small.copyWith(
                    color: Theme.of(context).colorScheme.onSecondary,
                  ),
                ),
              ),
              onPressed: () {
                BlocProvider.of<CategoriesSinglePageLoaderCubit>(ctx)
                    .loadNextPage();
              },
              child: const Text('Загрузить ещё'),
            ),
          ),
        ),
        if (state.items.isNotEmpty)
          Column(
            children: [
              const SizedBox(height: 2.0 * kStep),
              NumberPaginator(
                totalPages: (state.total / state.limit).ceil(),
                currentPage: state.page - kDefaultPaginationStartingPage,
                onPressed: (page) {
                  BlocProvider.of<CategoriesSinglePageLoaderCubit>(ctx)
                      .loadPage(page + kDefaultPaginationStartingPage);
                },
              ),
            ],
          ),
      ],
    );
  }

  Widget row2(Category c1, Category? c2) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      const aspectRatio = 140 / 200;
      final width = (ctrx.maxWidth - kStep) / 2.0;
      final height = width / aspectRatio;

      return IntrinsicHeight(
        child: Row(
          children: [
            Expanded(child: item(c1, width: width, height: height)),
            const SizedBox(width: 2.0 * kStep),
            if (c2 != null)
              Expanded(child: item(c2, width: width, height: height))
            else
              const Spacer(),
          ],
        ),
      );
    });
  }

  Widget row1(Category c) {
    return LayoutBuilder(builder: (ctx, ctrx) {
      const aspectRatio = 288 / 160;
      final width = ctrx.maxWidth;
      final height = width / aspectRatio;
      return item(c, width: width, height: height);
    });
  }

  Widget item(
    Category c, {
    required double width,
    required double height,
  }) {
    return Builder(builder: (context) {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          if (widget.breadcrumbs.length == 3) {
            context.push(
              CategoryItemsScreen.name,
              extra: List<Category>.from(widget.breadcrumbs)..add(c),
            );
          } else {
            context.push(
              CategoriesScreen.name,
              extra: List<Category>.from(widget.breadcrumbs)..add(c),
            );
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: width,
              height: height,
              child: ProductImage(c.avatarId),
            ),
            const SizedBox(height: kStep),
            Text(
              c.title,
              style: TextStyles.h2,
            ),
          ],
        ),
      );
    });
  }
}
