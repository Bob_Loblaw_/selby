part of 'categories_loader.dart';

class CategoriesSinglePageLoaderCubit extends Cubit<PaginationState<Category>> {
  CategoriesSinglePageLoaderCubit(int limit)
      : super(PaginationState(
          status: PaginationStatus.init,
          items: [],
          total: -1,
          limit: limit,
          page: -1,
          lastError: null,
          canLoadMore: false,
        ));

  @override
  void emit(PaginationState<Category> state) {
    if (isClosed) return;
    super.emit(state);
  }

  void loadPage(int page) async {
    if (state.status.isLoading) return;
    emit(state.copyWith(
      lastError: null,
      status: PaginationStatus.loading,
    ) as PaginationState<Category>);
    final res = await getIt<CategoriesApi>().getCategories(
      page,
      limit: state.limit,
    );
    if (res.success) {
      emit(state.copyWith(
        status: PaginationStatus.success,
        page: page,
        items: res.data.data,
        lastError: null,
        canLoadMore: res.data.total > res.data.page * res.data.limit,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Category>);
    } else {
      emit(state.copyWith(
        status: PaginationStatus.failure,
        lastError: res.error,
        canLoadMore: false,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Category>);
    }
  }

  void loadNextPage() async {
    if (state.status.isLoading) return;
    emit(state.copyWith(
      lastError: null,
      status: PaginationStatus.loading,
    ) as PaginationState<Category>);
    final res = await getIt<CategoriesApi>().getCategories(
      state.page + 1,
      limit: state.limit,
    );
    if (res.success) {
      emit(state.copyWith(
        status: PaginationStatus.success,
        page: state.page + 1,
        items: List<Category>.from(state.items)..addAll(res.data.data),
        lastError: null,
        canLoadMore: res.data.total > res.data.page * res.data.limit,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Category>);
    } else {
      emit(state.copyWith(
        status: PaginationStatus.failure,
        lastError: res.error,
        canLoadMore: false,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Category>);
    }
  }
}
