import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selby/common/common.dart';
import 'package:selby/shared/api/api.dart';
import 'package:selby/utils/utils.dart';

part 'categories_single_page_loader.dart';

class CategoriesLoaderCubit extends PaginationCubit<Category> {
  CategoriesLoaderCubit(int limit) : super(limit: limit);

  @override
  Future<Result<Pagination<Category>>> apiCall(int page) {
    return getIt<CategoriesApi>().getCategories(page, limit: state.limit);
  }
}
