import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

sealed class OAuthState {}

class OAuthInit extends OAuthState {}

class OAuthLoading extends OAuthState {}

class OAuthSuccess extends OAuthState {
  OAuthSuccess();
}

class OAuthFailure extends OAuthState {
  final String desc;

  OAuthFailure(this.desc);
}

class GoogleOAuthCubit extends Cubit<OAuthState> {
  GoogleOAuthCubit() : super(OAuthInit());

  Future<GoogleSignInAccount> _getGSIData() async {
    final gsi = GoogleSignIn(scopes: ['email']);
    final data = await gsi.signIn();
    return data!;
  }

  void signInWithGoogle() async {
    if (state is OAuthLoading) return;
    emit(OAuthLoading());

    try {
      final data = await _getGSIData();

      //todo do an api call to server and log in
      throw UnimplementedError();
    } catch (e) {
      return emit(OAuthFailure(e.toString()));
    }
  }
}

class AppleOAuthCubit extends Cubit<OAuthState> {
  AppleOAuthCubit() : super(OAuthInit());

  void signInWithApple() async {
    if (state is OAuthLoading) return;
    emit(OAuthLoading());

    try {
      if (!Platform.isIOS) {
        await SignInWithApple.getAppleIDCredential(
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName,
          ],
          webAuthenticationOptions: WebAuthenticationOptions(
            clientId: '', //todo supply your client id
            redirectUri: Uri.parse(''), //todo supply your redirect url
          ),
        );
        return;
      }

      final credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );
      final id = credential.identityToken;
      if (id == null) {
        throw 'Ошибка коммуникации с Apple'; //todo supply your own message
      }

      //todo do an api call to server and log in
      throw UnimplementedError();
    } catch (e) {
      return emit(OAuthFailure(e.toString()));
    }
  }
}
