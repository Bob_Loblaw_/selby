import 'package:selby/screens/auth/cubits/oauth/cubits.dart';
import 'package:selby/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class _OAuthOption extends StatelessWidget {
  final String icon;
  final bool loading;
  final VoidCallback onTap;

  const _OAuthOption({
    Key? key,
    required this.icon,
    required this.onTap,
    required this.loading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(12),
          ),
          border: Border.all(
            color: Colors.white,
            width: 1.0,
          ),
        ),
        child: Center(
          child: loading
              ? const SizedBox.square(
                  dimension: 25.0,
                  child: CircularProgressIndicator(),
                )
              : SvgPicture.asset(icon),
        ),
      ),
    );
  }
}

class GoogleOAuthButton extends StatelessWidget {
  const GoogleOAuthButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<GoogleOAuthCubit>(
      create: (context) => GoogleOAuthCubit(),
      child: BlocConsumer<GoogleOAuthCubit, OAuthState>(
        listener: (context, state) {
          switch (state) {
            case OAuthSuccess():
              {
                // todo nav away
              }
            case OAuthFailure():
              {
                // todo show error
              }
            case OAuthInit() || OAuthLoading():
              {}
          }
        },
        builder: (context, state) {
          return _OAuthOption(
            icon: SvgIcons.google,
            onTap: () {
              BlocProvider.of<GoogleOAuthCubit>(context).signInWithGoogle();
            },
            loading: state is OAuthLoading,
          );
        },
      ),
    );
  }
}

class AppleOAuthButton extends StatelessWidget {
  const AppleOAuthButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppleOAuthCubit>(
      create: (context) => AppleOAuthCubit(),
      child: BlocConsumer<AppleOAuthCubit, OAuthState>(
        listener: (context, state) {
          switch (state) {
            case OAuthSuccess():
              {
                // todo nav away
              }
            case OAuthFailure():
              {
                // todo show error
              }
            case OAuthInit() || OAuthLoading():
              {}
          }
        },
        builder: (context, state) {
          return _OAuthOption(
            icon: SvgIcons.apple,
            onTap: () {
              BlocProvider.of<AppleOAuthCubit>(context).signInWithApple();
            },
            loading: state is OAuthLoading,
          );
        },
      ),
    );
  }
}
