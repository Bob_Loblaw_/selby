import 'package:flutter/material.dart';

import 'package:go_router/go_router.dart';

import 'package:selby/screens/screens.dart';

class SetupScreen extends StatefulWidget {
  const SetupScreen({Key? key}) : super(key: key);

  @override
  State<SetupScreen> createState() => _SetupScreenState();
}

class _SetupScreenState extends State<SetupScreen> {
  @override
  void initState() {
    Future(() async {
      if (!mounted) return;
      context.go(HomeScreen.name);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Selby'),
      ),
    );
  }
}
