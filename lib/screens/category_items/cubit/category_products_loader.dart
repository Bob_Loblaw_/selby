import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:selby/common/common.dart';
import 'package:selby/shared/api/api.dart';
import 'package:selby/utils/utils.dart';

part 'category_products_single_page_loader.dart';

class CategoryProductsLoaderCubit extends PaginationCubit<Product> {
  final String categoryId;

  CategoryProductsLoaderCubit(
    this.categoryId, {
    required int limit,
  }) : super(limit: limit);

  @override
  Future<Result<Pagination<Product>>> apiCall(int page) {
    return getIt<CategoryListingApi>()
        .getCategoryListing(categoryId, page, limit: state.limit);
  }
}
