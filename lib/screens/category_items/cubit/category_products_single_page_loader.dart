part of 'category_products_loader.dart';

class CategoryProductsSinglePageLoaderCubit
    extends Cubit<PaginationState<Product>> {
  final String categoryId;

  CategoryProductsSinglePageLoaderCubit(
    this.categoryId, {
    required int limit,
  }) : super(PaginationState(
          status: PaginationStatus.init,
          items: [],
          total: -1,
          limit: limit,
          page: -1,
          lastError: null,
          canLoadMore: false,
        ));

  @override
  void emit(PaginationState<Product> state) {
    if (isClosed) return;
    super.emit(state);
  }

  void loadPage(int page) async {
    if (state.status.isLoading) return;
    emit(state.copyWith(
      lastError: null,
      status: PaginationStatus.loading,
    ) as PaginationState<Product>);
    final res = await getIt<CategoryListingApi>()
        .getCategoryListing(categoryId, page, limit: state.limit);
    if (res.success) {
      emit(state.copyWith(
        status: PaginationStatus.success,
        page: page,
        items: res.data.data,
        lastError: null,
        canLoadMore: res.data.total > res.data.page * res.data.limit,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Product>);
    } else {
      emit(state.copyWith(
        status: PaginationStatus.failure,
        lastError: res.error,
        canLoadMore: false,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Product>);
    }
  }

  void loadNextPage() async {
    if (state.status.isLoading) return;
    emit(state.copyWith(
      lastError: null,
      status: PaginationStatus.loading,
    ) as PaginationState<Product>);
    final res = await getIt<CategoryListingApi>().getCategoryListing(
      categoryId,
      state.page + 1,
      limit: state.limit,
    );
    if (res.success) {
      emit(state.copyWith(
        status: PaginationStatus.success,
        page: state.page + 1,
        items: List<Product>.from(state.items)..addAll(res.data.data),
        lastError: null,
        canLoadMore: res.data.total > res.data.page * res.data.limit,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Product>);
    } else {
      emit(state.copyWith(
        status: PaginationStatus.failure,
        lastError: res.error,
        canLoadMore: false,
        limit: state.limit,
        total: res.data.total,
      ) as PaginationState<Product>);
    }
  }
}
