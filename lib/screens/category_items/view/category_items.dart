import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:selby/common/common.dart';
import 'package:selby/screens/screens.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

class CategoryItemsScreen extends StatefulWidget {
  final List<Category> breadcrumbs;

  CategoryItemsScreen({
    Key? key,
    required this.breadcrumbs,
  })  : assert(breadcrumbs.isNotEmpty),
        super(key: key);

  static const String name = '/categoryItems';

  @override
  State<CategoryItemsScreen> createState() => _CategoryItemsScreenState();
}

class _CategoryItemsScreenState extends State<CategoryItemsScreen> {
  final sc = ScrollController();

  @override
  void dispose() {
    sc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CategoryProductsSinglePageLoaderCubit>(
      create: (_) => CategoryProductsSinglePageLoaderCubit(
        widget.breadcrumbs.last.id,
        limit: 14,
      )..loadPage(kDefaultPaginationStartingPage),
      child: ColoredBox(
        color: Theme.of(context).colorScheme.background,
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).padding.top + 2.0 * kStep),
            const SearchHeader(),
            const SizedBox(height: 2.0 * kStep),
            BreadcrumbsListing(breadcrumbs: widget.breadcrumbs),
            Expanded(child: pagination()),
          ],
        ),
      ),
    );
  }

  Widget pagination() {
    return BlocConsumer<CategoryProductsSinglePageLoaderCubit,
        PaginationState<Product>>(
      listener: (ctx, state) {
        if (state.status.isSuccess && state.items.length <= state.limit) {
          // if we loaded a page with a paginator widget
          WidgetsBinding.instance.addPostFrameCallback((_) {
            // give it time to settle in
            sc.animateTo(
              0,
              duration: const Duration(milliseconds: 750),
              curve: Curves.easeOutExpo,
            );
          });
        }
      },
      builder: (ctx, state) {
        return Stack(
          children: [
            Positioned.fill(child: list(ctx, state)),
            Positioned.fill(
              child: AnimatedOpacity(
                duration: kThemeChangeDuration,
                opacity: state.status.isLoading ? 1.0 : .0,
                child: IgnorePointer(
                  key: ValueKey(state.status.index),
                  ignoring: !state.status.isLoading,
                  child: ColoredBox(
                    color: Theme.of(ctx).colorScheme.background.withOpacity(.5),
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget list(BuildContext ctx, PaginationState<Product> state) {
    final l = state.items.length;

    return ListView.builder(
      controller: sc,
      padding: const EdgeInsets.all(2.0 * kStep),
      itemCount: l + 1,
      itemBuilder: (ctx, ind) {
        if (ind == l) {
          return paginationControl(ctx, state);
        } else {
          if (ind & 1 == 1) return const SizedBox();

          ind = ind >> 1;
          final l = state.items.length;
          final p1 = state.items[ind];
          final p2 = ind + 1 == l ? null : state.items[ind + 1];

          return Padding(
            padding: const EdgeInsets.only(bottom: kStep),
            child: ShopItemGridRow(p1: p1, p2: p2),
          );
        }
      },
    );
  }

  Widget paginationControl(BuildContext ctx, PaginationState<Product> state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Visibility(
          visible: state.canLoadMore,
          child: Padding(
            padding: const EdgeInsets.only(top: 2.0 * kStep),
            child: SizedBox(
              width: double.infinity,
              child: FilledButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Theme.of(ctx).colorScheme.secondary,
                  ),
                  textStyle: MaterialStateProperty.all(
                    TextStyles.small.copyWith(
                      color: Theme.of(ctx).colorScheme.onSecondary,
                    ),
                  ),
                ),
                onPressed: () {
                  BlocProvider.of<CategoryProductsSinglePageLoaderCubit>(ctx)
                      .loadNextPage();
                },
                child: const Text('Загрузить ещё'),
              ),
            ),
          ),
        ),
        if (state.items.isNotEmpty)
          Column(
            children: [
              const SizedBox(height: 2.0 * kStep),
              NumberPaginator(
                totalPages: (state.total / state.limit).ceil(),
                currentPage: state.page - kDefaultPaginationStartingPage,
                onPressed: (page) {
                  BlocProvider.of<CategoryProductsSinglePageLoaderCubit>(ctx)
                      .loadPage(page + kDefaultPaginationStartingPage);
                },
              ),
            ],
          ),
      ],
    );
  }
}
