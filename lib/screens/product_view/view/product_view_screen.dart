import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/models/models.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

part 'sliver_header_delegate.dart';
part 'title_and_price.dart';

part 'description.dart';
part 'characteristics.dart';
part 'reviews.dart';

class ProductViewScreen extends StatefulWidget {
  final Product product;

  const ProductViewScreen({
    Key? key,
    required this.product,
  }) : super(key: key);

  static const String name = '/product';

  @override
  State<ProductViewScreen> createState() => _ProductViewScreenState();

  static _ProductViewScreenState _of(BuildContext context) {
    return context.findAncestorStateOfType<_ProductViewScreenState>()!;
  }
}

class _ProductViewScreenState extends State<ProductViewScreen>
    with WidgetsBindingObserver {
  final selectedTab = ValueNotifier<int>(0);
  final gk = GlobalKey();
  double? height;

  void measure() {
    height = null;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        height = gk.currentContext?.size?.height;
      });
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    measure();
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void didUpdateWidget(covariant ProductViewScreen oldWidget) {
    measure();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeTextScaleFactor() {
    measure();
    super.didChangeTextScaleFactor();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    selectedTab.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(builder: (ctx, ctrx) {
        if (height == null) {
          return Offstage(
            offstage: true,
            child: _TitleAndPrice(
              key: gk,
              title: widget.product.label,
              price: widget.product.price,
            ),
          );
        }

        final mq = MediaQuery.of(ctx);

        return Stack(
          children: [
            Positioned.fill(
              child: NotificationListener<ScrollNotification>(
                onNotification: (not) {
                  final fs = FocusScope.of(context);
                  if (fs.hasFocus) fs.unfocus();
                  return false;
                },
                child: CustomScrollView(
                  slivers: [
                    SliverPersistentHeader(
                      delegate: ProductViewSliverDelegate(
                        product: widget.product,
                        topInsets: mq.padding.top,
                        titleAndPriceHeight: height!,
                        pagerHeight: ctrx.maxWidth,
                        pics: [
                          if (widget.product.coverId != null)
                            widget.product.coverId!,
                          if (widget.product.coverId != null)
                            widget.product.coverId!,
                          if (widget.product.coverId != null)
                            widget.product.coverId!,
                          if (widget.product.coverId != null)
                            widget.product.coverId!,
                        ],
                      ),
                      floating: false,
                      pinned: true,
                    ),
                    SliverPadding(
                      padding: EdgeInsets.only(
                        top: 2.0 * kStep,
                        bottom: MediaQuery.of(context).padding.bottom +
                            2.0 * kStep +
                            BuyArea.height +
                            3.0 * kStep,
                      ),
                      sliver: SliverToBoxAdapter(
                        child: ColoredBox(
                          color: Theme.of(context).colorScheme.background,
                          child: selectedDetails(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: mq.padding.bottom + 2.0 * kStep,
              height: BuyArea.height,
              left: 2.0 * kStep,
              right: 2.0 * kStep,
              child: const BuyArea(),
            ),
          ],
        );
      }),
    );
  }

  Widget selectedDetails() {
    return ListenableBuilder(
      listenable: selectedTab,
      builder: (_, __) {
        return AnimatedSwitcher(
          duration: kThemeChangeDuration,
          child: switch (selectedTab.value) {
            0 => _ProductDescription(product: widget.product),
            1 => _ProductCharacteristics(product: widget.product),
            2 => _ProductReviews(product: widget.product),
            _ => throw UnsupportedError('wrong tab index'),
          },
        );
      },
    );
  }
}

class ProductViewActionBar extends StatelessWidget {
  static const double height = 48.0;

  const ProductViewActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: height,
      child: Row(
        children: [
          const SizedBox(width: kStep),
          HapticArea(
            borderRadius: const BorderRadius.all(
              Radius.circular(height / 2),
            ),
            onTap: () {
              context.pop();
            },
            padding: EdgeInsets.zero,
            child: SizedBox.square(
              dimension: height,
              child: Center(child: SvgPicture.asset(SvgIcons.back)),
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}

class BuyArea extends StatefulWidget {
  final int? limit;

  static const height = 72.0;

  const BuyArea({
    Key? key,
    this.limit,
  }) : super(key: key);

  @override
  State<BuyArea> createState() => _BuyAreaState();
}

class _BuyAreaState extends State<BuyArea> {
  final count = ValueNotifier<int>(1);
  final controller = TextEditingController(text: '1');

  bool get canIncrease {
    final newValue = count.value + 1;
    final lim = widget.limit;
    if (lim == null) return true;
    return newValue <= lim;
  }

  bool get canDecrease {
    final newValue = count.value - 1;
    return newValue > 0;
  }

  @override
  void initState() {
    count.addListener(() {
      final s = count.value.toString();
      if (controller.text == s) return;
      controller.value = TextEditingValue(
        text: s,
        selection: TextSelection.collapsed(
          offset: s.length,
        ),
      );
    });

    controller.addListener(() {
      final val = int.tryParse(controller.text);
      if (val == null) return;
      count.value = val;
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    count.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: BuyArea.height,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(kBorderRadiusBig),
          ),
          color: Theme.of(context).colorScheme.surface,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 2.0 * kStep,
            vertical: kStep,
          ),
          child: Row(
            children: [
              amountPicker(),
              const Spacer(),
              button(),
            ],
          ),
        ),
      ),
    );
  }

  Widget amountPicker() {
    return LayoutBuilder(builder: (ctx, ctrx) {
      return SizedBox(
        height: 44.0,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.background,
            borderRadius: const BorderRadius.all(
              Radius.circular(kBorderRadiusBig),
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              HapticArea(
                borderRadius: const BorderRadius.all(
                  Radius.circular(18.0),
                ),
                padding: EdgeInsets.zero,
                onTap: () {
                  if (canDecrease) count.value = count.value - 1;
                },
                child: ListenableBuilder(
                  listenable: count,
                  builder: (_, __) => countIcon(
                    ctrx,
                    SvgIcons.minus,
                    canDecrease,
                  ),
                ),
              ),
              amountInputField(),
              HapticArea(
                borderRadius: const BorderRadius.all(
                  Radius.circular(18.0),
                ),
                padding: EdgeInsets.zero,
                onTap: () {
                  if (canIncrease) count.value = count.value + 1;
                },
                child: ListenableBuilder(
                  listenable: count,
                  builder: (_, __) => countIcon(
                    ctrx,
                    SvgIcons.plus,
                    canIncrease,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget countIcon(BoxConstraints ctrx, String path, bool primary) {
    return SizedBox.square(
      dimension: ctrx.maxHeight,
      child: Center(
        child: SizedBox.square(
          dimension: 24.0,
          child: SvgPicture.asset(
            path,
            color: primary
                ? Theme.of(context).colorScheme.primary
                : Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      ),
    );
  }

  Widget amountInputField() {
    return SizedBox(
      width: 48.0,
      height: double.infinity,
      child: Center(
        child: TextFormField(
          controller: controller,
          style: TextStyles.normalSemibold,
          textAlign: TextAlign.center,
          decoration: const InputDecoration(
            contentPadding: EdgeInsets.zero,
            isDense: true,
            isCollapsed: true,
            border: InputBorder.none,
            disabledBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            focusedErrorBorder: InputBorder.none,
          ),
          keyboardType: const TextInputType.numberWithOptions(
            signed: false,
            decimal: false,
          ),
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
            MinLengthInputFormatter(1),
            LengthLimitingTextInputFormatter(3),
          ],
        ),
      ),
    );
  }

  Widget button() {
    return SizedBox(
      height: 44.0,
      child: FilledButton(
        onPressed: () {},
        child: const Text('Заказать'),
      ),
    );
  }
}

class _ProductTabs extends StatelessWidget {
  const _ProductTabs({Key? key}) : super(key: key);

  static const double height = 48.0;

  @override
  Widget build(BuildContext context) {
    final cs = Theme.of(context).colorScheme;
    final selectedTab = ProductViewScreen._of(context).selectedTab;

    Widget animated(String title, int index) {
      return TweenAnimationBuilder<double>(
        tween: Tween<double>(
          begin: selectedTab.value == index ? 1.0 : 0.0,
          end: selectedTab.value == index ? 1.0 : 0.0,
        ),
        duration: kThemeChangeDuration,
        builder: (ctx, t, __) {
          return GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              selectedTab.value = index;
            },
            child: SizedBox(
              height: double.infinity,
              child: Center(
                child: Text(
                  title,
                  style: TextStyles.h2.copyWith(
                    color: Color.lerp(
                      cs.onSecondary,
                      cs.primary,
                      t,
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      );
    }

    return SizedBox(
      height: height,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(
          left: 2.0 * kStep,
          right: 3.0 * kStep,
        ),
        child: ListenableBuilder(
          listenable: selectedTab,
          builder: (_, __) {
            return Row(
              children: [
                animated('Описание', 0),
                const SizedBox(width: 2.0 * kStep),
                animated('Характеристики', 1),
                /*const SizedBox(width: 2.0 * kStep),
                animated('Отзывы', 2),*/
              ],
            );
          },
        ),
      ),
    );
  }
}
