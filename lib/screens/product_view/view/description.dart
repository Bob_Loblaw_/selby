part of 'product_view_screen.dart';

class _ProductDescription extends StatefulWidget {
  final Product product;

  const _ProductDescription({
    Key? key,
    required this.product,
  }) : super(key: key);

  /// How many lines in description are fine before we need
  /// to cut it and hide part of it under `show more`
  static const int spoilerThreshold = 5;

  @override
  State<_ProductDescription> createState() => _ProductDescriptionState();
}

class _ProductDescriptionState extends State<_ProductDescription> {
  final open = ValueNotifier<bool>(false);

  @override
  void dispose() {
    open.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final desc = widget.product.descriptionShort;
    if (desc == null) return const SizedBox();

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 2.0 * kStep,
      ),
      child: LayoutBuilder(
        builder: (ctx, ctrx) {
          final numLines = getNumLines(ctx, ctrx);

          if (numLines <= _ProductDescription.spoilerThreshold) {
            return Text(
              desc,
              style: TextStyles.normal,
            );
          }

          return ListenableBuilder(
            listenable: open,
            builder: (ctx, _) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AnimatedSwitcher(
                    duration: kThemeChangeDuration,
                    child: open.value
                        ? Text(
                            desc,
                            key: const ValueKey(true),
                            style: TextStyles.normal,
                          )
                        : Text(
                            desc,
                            key: const ValueKey(false),
                            style: TextStyles.normal,
                            maxLines: _ProductDescription.spoilerThreshold,
                            overflow: TextOverflow.ellipsis,
                          ),
                  ),
                  const SizedBox(height: kStep * .5),
                  showHide(),
                ],
              );
            },
          );
        },
      ),
    );
  }

  int getNumLines(BuildContext ctx, BoxConstraints ctrx) {
    final textDir = Directionality.of(context);
    final tsf = MediaQuery.textScaleFactorOf(context);

    final span = TextSpan(
      text: widget.product.descriptionShort,
      style: TextStyles.normal,
    );

    final tp = TextPainter(
      text: span,
      textDirection: textDir,
      textScaleFactor: tsf,
    );

    tp.layout(maxWidth: ctrx.maxWidth);
    return tp.computeLineMetrics().length;
  }

  Widget showHide() {
    return IntrinsicWidth(
      child: Transform.translate(
        offset: const Offset(-kStep, 0),
        child: HapticArea(
          onTap: () {
            open.value = !open.value;
          },
          child: SizedBox(
            height: 40.0,
            child: Align(
              alignment: AlignmentDirectional.centerStart,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    open.value ? 'Скрыть' : 'Показать',
                    style: TextStyles.normal.copyWith(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                  const SizedBox(width: kStep),
                  TweenAnimationBuilder<double>(
                    tween: Tween(
                      begin: open.value ? 1.0 : 0.0,
                      end: open.value ? 1.0 : 0.0,
                    ),
                    duration: kThemeChangeDuration,
                    builder: (ctx, val, _) {
                      return Transform.rotate(
                        angle: -math.pi * val,
                        child: SvgPicture.asset(
                          SvgIcons.chevronDown,
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
