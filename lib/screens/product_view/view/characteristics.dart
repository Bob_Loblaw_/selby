part of 'product_view_screen.dart';

class _ProductCharacteristics extends StatelessWidget {
  final Product product;

  const _ProductCharacteristics({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final a = product.attributes;
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 2.0 * kStep,
      ),
      child: Column(
        children: [
          for (int i = 0; i < (a.length << 1) - 1; i++)
            (i & 1 == 1) ? const SizedBox(height: kStep) : item(a[i >> 1])
        ],
      ),
    );
  }

  Widget item(EavValue eav) {
    final key = eav.key;
    final value = eav.value;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              key,
              style: TextStyles.normal,
            ),
          ),
        ),
        const SizedBox(width: 2.0 * kStep),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              value is num ? value.toCurrencyString() : value.toString(),
              style: TextStyles.normal,
              textAlign: TextAlign.end,
            ),
          ),
        ),
      ],
    );
  }
}
