part of 'product_view_screen.dart';

class ProductViewSliverDelegate extends SliverPersistentHeaderDelegate {
  final Product product;
  final List<String> pics;
  final double topInsets;
  final double titleAndPriceHeight;
  final double pagerHeight;

  ProductViewSliverDelegate({
    required this.product,
    required this.pics,
    required this.topInsets,
    required this.titleAndPriceHeight,
    required this.pagerHeight,
  });

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    final delta = maxExtent - minExtent;
    final opacity = (shrinkOffset / delta).clamp(0.0, 1.0);

    return Stack(
      children: [
        Positioned(
          top: 0.0,
          left: 0.0,
          right: 0.0,
          bottom: bottomContentHeight - kBorderRadiusBig,
          child: ClipRect(
            child: IgnorePointer(
              ignoring: opacity > .2,
              child: _PicsPager(
                pics: pics,
              ),
            ),
          ),
        ),
        Positioned(
          top: 0.0,
          left: 0.0,
          right: 0.0,
          bottom: bottomContentHeight - kBorderRadiusBig,
          child: ClipRect(
            child: IgnorePointer(
              ignoring: true,
              child: Opacity(
                opacity: opacity,
                child: ColoredBox(
                  color: Theme.of(context).colorScheme.background,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 0.0,
          right: 0.0,
          top: 2.0 * kStep + topInsets,
          height: ProductViewActionBar.height,
          child: const ProductViewActionBar(),
        ),
        Positioned(
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
          height: titleAndPriceHeight + 2.0 * kStep + _ProductTabs.height + 1.0,
          child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(kBorderRadiusBig),
                topRight: Radius.circular(kBorderRadiusBig),
              ),
              color: Theme.of(context).colorScheme.background,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _TitleAndPrice(
                  title: product.label,
                  price: product.price,
                ),
                const SizedBox(height: 2.0 * kStep),
                const _ProductTabs(),
                Divider(
                  height: 1.0,
                  thickness: 1.0,
                  color: Theme.of(context).colorScheme.onSecondary,
                  indent: 2.0 * kStep,
                  endIndent: 2.0 * kStep,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  double get bottomContentHeight =>
      titleAndPriceHeight + 2.0 * kStep + _ProductTabs.height + 1.0;

  @override
  double get maxExtent => pagerHeight + bottomContentHeight;

  @override
  double get minExtent =>
      topInsets +
      2.0 * kStep +
      ProductViewActionBar.height +
      bottomContentHeight;

  @override
  bool shouldRebuild(
    covariant SliverPersistentHeaderDelegate oldDelegate,
  ) {
    return false;
  }
}

class _PicsPager extends StatefulWidget {
  final List<String> pics;

  const _PicsPager({
    Key? key,
    required this.pics,
  }) : super(key: key);

  @override
  State<_PicsPager> createState() => _PicsPagerState();
}

class _PicsPagerState extends State<_PicsPager> {
  final c = PageController(initialPage: 0);
  final current = ValueNotifier<int>(0);

  @override
  void initState() {
    c.addListener(() {
      final page = c.page;
      if (page == null) return;
      current.value = page.round();
    });
    super.initState();
  }

  @override
  void dispose() {
    c.dispose();
    current.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.pics.isEmpty) {
      return const ProductImage(
        null,
        placeholderScaleBaseDimension: ScaleBaseDimension.max,
      );
    }

    final dotsWidth = (widget.pics.length * 2 - 1) * kStep;

    return LayoutBuilder(builder: (ctx, ctrx) {
      return Stack(
        children: [
          Positioned.fill(
            child: PageView(
              controller: c,
              children: widget.pics.take(7).map<Widget>((e) {
                return ProductImage(e, fit: BoxFit.cover);
              }).toList(),
            ),
          ),
          Positioned(
            bottom: 16.0 + kBorderRadiusBig,
            height: kStep,
            width: dotsWidth,
            left: (ctrx.maxWidth - dotsWidth) / 2.0,
            child: ListenableBuilder(
              listenable: current,
              builder: (ctx, _) {
                return PagerProgressDots(
                  size: widget.pics.length.clamp(0, 7),
                  current: current.value,
                );
              },
            ),
          ),
        ],
      );
    });
  }
}
