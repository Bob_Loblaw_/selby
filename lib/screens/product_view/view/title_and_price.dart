part of 'product_view_screen.dart';

class _TitleAndPrice extends StatelessWidget {
  final String title;
  final num price;

  const _TitleAndPrice({
    Key? key,
    required this.title,
    required this.price,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 2.0 * kStep,
        left: 2.0 * kStep,
        right: 3.0 * kStep,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: TextStyles.h1),
          const SizedBox(height: kStep),
          Text(
            price.toCurrencyString(suffix: kRubUnicode),
            style: TextStyles.h1,
          ),
        ],
      ),
    );
  }
}
