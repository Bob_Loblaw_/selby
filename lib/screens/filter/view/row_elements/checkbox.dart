part of '../filter_screen.dart';

class CheckboxRow extends StatelessWidget {
  final String title;
  final bool toggled;
  final void Function(bool) onToggled;
  final EdgeInsets padding;

  const CheckboxRow({
    Key? key,
    required this.title,
    required this.toggled,
    required this.onToggled,
    this.padding = EdgeInsets.zero,
  }) : super(key: key);

  static const double height = 40.0;

  @override
  Widget build(BuildContext context) {
    return HapticArea(
      padding: padding,
      onTap: () {
        onToggled(!toggled);
      },
      child: SizedBox(
        width: double.infinity,
        height: height,
        child: Row(
          children: [
            SelbyCheckbox(
              toggled: toggled,
              onToggled: onToggled,
            ),
            const SizedBox(width: 18.0),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  title,
                  style: TextStyles.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SelbyCheckbox extends StatelessWidget {
  final bool toggled;
  final void Function(bool) onToggled;

  const SelbyCheckbox({
    Key? key,
    required this.toggled,
    required this.onToggled,
  }) : super(key: key);

  static const double dimension = 20.0;

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: dimension,
      child: TweenAnimationBuilder<double>(
        duration: kThemeChangeDuration,
        tween: Tween<double>(
          begin: toggled ? 1.0 : 0.0,
          end: toggled ? 1.0 : 0.0,
        ),
        builder: (ctx, value, _) {
          return CustomPaint(
            painter: _CheckboxPainter(
              value,
              cs: Theme.of(context).colorScheme,
            ),
          );
        },
      ),
    );
  }
}

class _CheckboxPainter extends CustomPainter {
  final double progress;
  final ColorScheme cs;

  _CheckboxPainter(
    this.progress, {
    required this.cs,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final p = Paint();

    // fill rect
    p.color = Color.lerp(
      cs.background,
      cs.primaryContainer,
      progress,
    )!;
    p.style = PaintingStyle.fill;
    canvas.drawRect(Offset.zero & size, p);

    // draw rect border
    p.color = Color.lerp(cs.onBackground, cs.primary, progress)!;
    p.style = PaintingStyle.stroke;
    p.strokeWidth = 1.0;
    canvas.drawRect(Offset.zero & size, p);

    //draw checkmark
    p.color = cs.primary;
    p.style = PaintingStyle.stroke;
    p.strokeWidth = 2.0;
    p.strokeJoin = StrokeJoin.miter;

    canvas.translate(size.width - .8, -size.height / 4);
    canvas.rotate(math.pi / 4);

    final start1 = Offset(0, size.height);
    final end1 = Offset(size.width / 3.5 + p.strokeWidth / 2, size.height);
    final start2 = Offset(size.width / 3.5, size.height);
    final end2 = Offset(size.width / 3.5, size.height / 2);

    if (progress < .333) {
      final anim = Offset.lerp(start1, end1, progress)!;
      canvas.drawPoints(PointMode.lines, [start1, anim], p);
    } else {
      final anim = Offset.lerp(start2, end2, progress)!;
      canvas.drawPoints(PointMode.lines, [start1, end1, start2, anim], p);
    }
  }

  @override
  bool shouldRepaint(covariant _CheckboxPainter old) {
    return old.progress != progress || old.cs != cs;
  }
}
