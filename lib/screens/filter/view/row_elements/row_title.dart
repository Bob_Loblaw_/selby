part of '../filter_screen.dart';

class RowTitle extends StatelessWidget {
  final String title;
  final VoidCallback? onReset;

  const RowTitle({
    Key? key,
    required this.title,
    this.onReset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyles.small,
            ),
          ),
        ),
        Transform.translate(
          offset: const Offset(kStep, 0),
          child: HapticArea(
            borderRadius: const BorderRadius.all(
              Radius.circular(kBorderRadiusSmall),
            ),
            padding: const EdgeInsets.all(kStep),
            onTap: onReset,
            child: const Text('Сбросить'),
          ),
        ),
      ],
    );
  }
}
