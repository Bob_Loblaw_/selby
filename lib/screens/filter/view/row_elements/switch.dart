part of '../filter_screen.dart';

const _kCheckboxSize = Size(40.0, 20.0);

class SwitchRow extends StatelessWidget {
  final String title;
  final bool toggled;
  final void Function(bool) onToggled;

  const SwitchRow({
    Key? key,
    required this.title,
    required this.toggled,
    required this.onToggled,
  }) : super(key: key);

  static const double height = 40.0;

  @override
  Widget build(BuildContext context) {
    return HapticArea(
      onTap: () {
        onToggled(!toggled);
      },
      padding: const EdgeInsets.symmetric(
        horizontal: 2.0 * kStep,
      ),
      child: SizedBox(
        width: double.infinity,
        height: height,
        child: Row(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  title,
                  style: TextStyles.normal,
                ),
              ),
            ),
            const SizedBox(width: 18.0),
            CustomSwitch(
              onToggled: onToggled,
              isActive: toggled,
              appearance: SwitchAppearance.painted(
                trackSize: _kCheckboxSize,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
