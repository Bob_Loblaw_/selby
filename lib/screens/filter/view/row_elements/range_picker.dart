part of '../filter_screen.dart';

class RangePickerRow extends StatefulWidget {
  final String title;
  final RangeValues rangeValues;
  final void Function(RangeSetting) onChanged;

  const RangePickerRow({
    Key? key,
    required this.rangeValues,
    required this.onChanged,
    required this.title,
  }) : super(key: key);

  static const double height = kTextFieldHeight;

  @override
  State<RangePickerRow> createState() => _RangePickerRowState();
}

class _RangePickerRowState extends State<RangePickerRow> {
  final startController = TextEditingController();
  final endController = TextEditingController();

  void _set(String what, TextEditingController where) {
    where.value = TextEditingValue(
      text: what,
      selection: TextSelection.collapsed(
        offset: what.length,
      ),
    );
  }

  void _listener() {
    final s = widget.rangeValues.start.toCurrencyString();
    final e = widget.rangeValues.end.toCurrencyString();
    final st = startController.text.trim();
    final et = endController.text.trim();

    if (s != st || e != et) {
      widget.onChanged(RangeSetting(
        currentMin: st.isEmpty ? null : st.parseToNum(),
        currentMax: et.isEmpty ? null : et.parseToNum(),
      ));
    }
  }

  @override
  void initState() {
    _set(widget.rangeValues.start.toCurrencyString(), startController);
    _set(widget.rangeValues.end.toCurrencyString(), endController);

    startController.addListener(_listener);
    endController.addListener(_listener);
    super.initState();
  }

  @override
  void didUpdateWidget(covariant RangePickerRow old) {
    /*final s = widget.rangeValues.start.toCurrencyString();
    final e = widget.rangeValues.end.toCurrencyString();
    final st = startController.text.trim();
    final et = endController.text.trim();

    if (st != s) _set(s, startController);
    if (et != e) _set(e, endController);*/

    super.didUpdateWidget(old);
  }

  @override
  void dispose() {
    startController.dispose();
    endController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          height: 2.0,
          thickness: 1.0,
          color: Theme.of(context).colorScheme.onSecondary,
        ),
        RowTitle(title: widget.title),
        const SizedBox(height: kStep),
        range(),
        const SizedBox(height: kStep),
        inputs(),
      ],
    );
  }

  Widget range() {
    Widget el(String s1, String s2, AlignmentGeometry alignment) {
      return Expanded(
        child: Align(
          alignment: alignment,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                s1,
                style: TextStyles.small,
              ),
              Text(
                s2,
                style: TextStyles.small.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Row(
      children: [
        el(
          'От ',
          widget.rangeValues.start.toCurrencyString(suffix: kRubUnicode),
          Alignment.centerLeft,
        ),
        el(
          'До ',
          widget.rangeValues.end.toCurrencyString(suffix: kRubUnicode),
          Alignment.centerRight,
        ),
      ],
    );
  }

  Widget inputs() {
    return Row(
      children: [
        Expanded(
          child: _RangeInputField(
            controller: startController,
            range: widget.rangeValues,
          ),
        ),
        const SizedBox(width: kStep),
        Expanded(
          child: _RangeInputField(
            controller: endController,
            range: widget.rangeValues,
          ),
        ),
      ],
    );
  }
}

class _RangeInputField extends StatelessWidget {
  final TextEditingController controller;
  final RangeValues? range;

  const _RangeInputField({
    Key? key,
    required this.controller,
    required this.range,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final activeBorder = OutlineInputBorder(
      borderRadius: const BorderRadius.all(
        Radius.circular(32.0),
      ),
      borderSide: BorderSide(
        color: Theme.of(context).colorScheme.primary,
      ),
      gapPadding: 0.0,
    );

    return SizedBox(
      width: double.infinity,
      height: 64.0,
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          hintText: 'Введите значение',
          hintStyle: TextStyles.small,
          contentPadding: const EdgeInsets.symmetric(
            vertical: 4.0,
            horizontal: 16.0,
          ),
          focusedErrorBorder: activeBorder,
          disabledBorder: activeBorder,
          enabledBorder: activeBorder,
          focusedBorder: activeBorder,
          errorBorder: activeBorder,
          border: activeBorder,
        ),
        inputFormatters: [
          //FilteringTextInputFormatter.digitsOnly,
          CurrencyInputFormatter(
            thousandSeparator: ThousandSeparator.Space,
            mantissaLength: 0,
          ),
          /*NumRangeInputFormatter(
            min: range?.start,
            max: range?.end,
          ),*/
        ],
        /*keyboardType: const TextInputType.numberWithOptions(
          signed: false,
          decimal: false,
        ),*/
      ),
    );
  }
}

final class RangeSetting {
  final num? currentMin;
  final num? currentMax;

  const RangeSetting({
    this.currentMin,
    this.currentMax,
  });
}
