part of '../filter_screen.dart';

class MultichoiceRow extends StatefulWidget {
  final String title;
  final List<Choice> choices;
  final void Function(List<Choice> newList) onUpdated;
  final VoidCallback? onReset;

  const MultichoiceRow({
    Key? key,
    required this.title,
    required this.choices,
    required this.onUpdated,
    this.onReset,
  }) : super(key: key);

  static const double height = kTextFieldHeight;

  @override
  State<MultichoiceRow> createState() => _MultichoiceRowState();
}

class _MultichoiceRowState extends State<MultichoiceRow> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 2.0 * kStep,
          ),
          child: Column(
            children: [
              Divider(
                thickness: 1.0,
                height: 2.0,
                color: Theme.of(context).colorScheme.onSecondary,
              ),
              RowTitle(title: widget.title, onReset: widget.onReset),
            ],
          ),
        ),
        const SizedBox(height: kStep),
        SizedBox(
          height: 40.0,
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.symmetric(
              horizontal: 2.0 * kStep,
            ),
            itemBuilder: (ctx, ind) {
              return item(widget.choices[ind]);
            },
            separatorBuilder: (ctx, ind) {
              return const SizedBox(width: kStep);
            },
            itemCount: widget.choices.length,
          ),
        ),
        HapticArea(
          padding: const EdgeInsets.symmetric(
            vertical: kStep,
            horizontal: 2.0 * kStep,
          ),
          onTap: () {
            context.push(
              '${FilterScreen.name}/${FilterMultichoiceShowAll.name}',
              extra: {
                'title': widget.title,
                'choices': widget.choices,
              },
            ).then((res) {
              if (res is List<Choice>) {
                widget.onUpdated(res);
              }
            });
          },
          child: Text(
            'Показать все',
            style: TextStyles.small.copyWith(
              color: Theme.of(context).colorScheme.onSecondary,
            ),
          ),
        ),
      ],
    );
  }

  Widget item(Choice choice) {
    final cs = Theme.of(context).colorScheme;

    return ClipRRect(
      borderRadius: const BorderRadius.all(
        Radius.circular(42069),
      ),
      child: HapticArea(
        overlayColor: cs.primary.withOpacity(.5),
        onTap: () {
          final newList = widget.choices
              .map((e) => e == choice ? e.copyWith(chosen: !e.chosen) : e)
              .toList();
          widget.onUpdated(newList);
        },
        padding: EdgeInsets.zero,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(42069),
            ),
            border: Border.all(
              color: choice.chosen ? cs.primary : cs.onSecondary,
              width: 1.0,
            ),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 2.0 * kStep,
            vertical: kStep,
          ),
          child: Center(
            child: Text(
              choice.title,
              style: TextStyles.small.copyWith(
                color: choice.chosen ? cs.primary : cs.onBackground,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
