part of 'filter_screen.dart';

class _NavBar extends StatelessWidget {
  final String title;
  final VoidCallback? onBack;

  const _NavBar({
    Key? key,
    required this.title,
    this.onBack,
  }) : super(key: key);

  static const double height = 48.0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: double.infinity,
      child: Row(
        children: [
          HapticArea(
            onTap: onBack ??
                () {
                  context.pop();
                },
            child: Center(
              child: SvgPicture.asset(
                SvgIcons.back,
              ),
            ),
          ),
          const SizedBox(width: kStep),
          Expanded(
            child: Align(
              alignment: AlignmentDirectional.centerStart,
              child: Text(
                title,
                style: TextStyles.h2,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
