import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart' hide Switch;
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart'
    hide NumericInputFormatting;

import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

import 'package:selby/common/common.dart';
import 'package:selby/shared/widgets/widgets.dart';
import 'package:selby/utils/utils.dart';

part 'filter_multichoice_show_all.dart';
part 'nav_bar.dart';

part 'row_elements/checkbox.dart';
part 'row_elements/multichoice.dart';
part 'row_elements/range_picker.dart';
part 'row_elements/row_title.dart';
part 'row_elements/switch.dart';

class FilterScreen extends StatefulWidget {
  const FilterScreen({Key? key}) : super(key: key);

  static const String name = '/filter';

  @override
  State<FilterScreen> createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  final map = <String, dynamic>{
    'В наличии': true,
    'Со скидкой': true,
    'Стоимость, $kRubUnicode': const RangeValues(200.0, 21800.0),
    'Бренд': <Choice>[
      for (int i = 0; i < 15; i++)
        Choice(
          title: 'Бренд $i',
          chosen: false,
        ),
    ],
  };

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Theme.of(context).colorScheme.background,
      child: SafeArea(
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.only(
                left: 0,
                top: 2.0 * kStep,
              ),
              child: _NavBar(title: 'Фильтры'),
            ),
            Divider(
              height: 1.0,
              thickness: 1.0,
              color: Theme.of(context).colorScheme.onSecondary,
            ),
            Expanded(child: filters()),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.0 * kStep),
              child: Column(
                children: [
                  Divider(
                    height: kStep,
                    thickness: 1.0,
                    color: Theme.of(context).colorScheme.onSecondary,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: () {},
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          Theme.of(context).colorScheme.surfaceVariant,
                        ),
                        textStyle: MaterialStateProperty.all(TextStyles.small),
                      ),
                      child: const Text('Показать 550 товаров'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget filters() {
    return ListView(
      children: map.entries.map<Widget>((e) {
        final MapEntry(key: k, value: v) = e;

        if (v is bool) {
          return SwitchRow(
            title: k,
            toggled: v,
            onToggled: (toggled) {
              setState(() {
                map[k] = toggled;
              });
            },
          );
        } else if (v is RangeValues) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 2.0 * kStep,
            ),
            child: RangePickerRow(
              onChanged: (setting) {
                setState(() {});
              },
              rangeValues: v,
              title: k,
            ),
          );
        } else if (v is List<Choice>) {
          return MultichoiceRow(
            title: k,
            choices: v,
            onUpdated: (newList) {
              setState(() {
                map[k] = newList;
              });
            },
            onReset: () {
              setState(() {
                map[k] = (map[k] as List<Choice>)
                    .map(
                      (e) => Choice(
                        title: e.title,
                        chosen: false,
                      ),
                    )
                    .toList();
              });
            },
          );
        }

        return const SizedBox();
      }).toList(),
    );
  }
}
