part of 'filter_screen.dart';

class FilterMultichoiceShowAll extends StatefulWidget {
  final String title;
  final List<Choice> choices;

  const FilterMultichoiceShowAll({
    Key? key,
    required this.title,
    required this.choices,
  }) : super(key: key);

  static const String name = 'multichoice_show_all';

  @override
  State<FilterMultichoiceShowAll> createState() =>
      _FilterMultichoiceShowAllState();
}

class _FilterMultichoiceShowAllState extends State<FilterMultichoiceShowAll> {
  late List<Choice> copy;

  @override
  void initState() {
    copy = List.from(widget.choices);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cs = Theme.of(context).colorScheme;

    return ColoredBox(
      color: cs.background,
      child: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 0,
                top: 2.0 * kStep,
              ),
              child: _NavBar(
                title: widget.title,
                onBack: onBack,
              ),
            ),
            Divider(
              height: 1.0,
              thickness: 1.0,
              color: cs.onSecondary,
            ),
            const Padding(
              padding: EdgeInsets.only(
                top: 2.0 * kStep,
                left: 2.0 * kStep,
                right: 2.0 * kStep,
              ),
              child: SearchField(),
            ),
            Expanded(
              child: LayoutBuilder(builder: (ctx, ctrx) {
                const pxSize = 3.0 * kStep;
                final percent = pxSize / ctrx.maxHeight;

                return Container(
                  foregroundDecoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        cs.background,
                        cs.background,
                        cs.background.withOpacity(0),
                        cs.background.withOpacity(0),
                      ],
                      stops: [0.0, percent / 2, percent, 1.0],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: listing(),
                );
              }),
            ),
            Divider(
              height: 1.0,
              thickness: 1.0,
              color: cs.onSecondary,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 2.0 * kStep,
                left: 2.0 * kStep,
                right: 2.0 * kStep,
                bottom: kStep,
              ),
              child: SizedBox(
                width: double.infinity,
                child: FilledButton(
                  onPressed: onBack,
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      cs.surfaceVariant,
                    ),
                    textStyle: MaterialStateProperty.all(TextStyles.small),
                  ),
                  child: const Text('Готово'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget listing() {
    return ListView.separated(
      padding: const EdgeInsets.symmetric(
        vertical: 2.0 * kStep,
      ),
      itemBuilder: (ctx, ind) {
        final item = copy[ind];
        return CheckboxRow(
          title: item.title,
          toggled: item.chosen,
          padding: const EdgeInsets.symmetric(
            horizontal: 2.0 * kStep,
          ),
          onToggled: (toggled) {
            setState(() {
              copy = copy
                  .map((e) => e.title == copy[ind].title
                      ? e.copyWith(chosen: !e.chosen)
                      : e)
                  .toList();
            });
          },
        );
      },
      separatorBuilder: (ctx, ind) {
        return const SizedBox(height: kStep);
      },
      itemCount: copy.length,
    );
  }

  void onBack() {
    context.pop(copy);
  }
}
