import 'package:freezed_annotation/freezed_annotation.dart';

part 'resource.freezed.dart';
part 'resource.g.dart';

@freezed
class Resource with _$Resource {
  const factory Resource({
    required String id,
    required String? name,
    required int? size,
    required String? creatorId,
    required String mimeType,
    required String? originalResourceId,
    required DateTime createdAt,
    required DateTime? deletedAt,
  }) = _Resource;

  factory Resource.fromJson(Map<String, dynamic> json) =>
      _$ResourceFromJson(json);
}
