import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:selby/common/models/models.dart';

part 'product.freezed.dart';
part 'product.g.dart';

@freezed
class Product with _$Product {
  const factory Product({
    required String id,
    required String label,
    required String? descriptionShort,
    required String? coverId,
    required num price,
    required List<EavValue> attributes,
  }) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}
