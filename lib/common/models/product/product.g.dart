// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Product _$$_ProductFromJson(Map<String, dynamic> json) => _$_Product(
      id: json['id'] as String,
      label: json['label'] as String,
      descriptionShort: json['descriptionShort'] as String?,
      coverId: json['coverId'] as String?,
      price: json['price'] as num,
      attributes: (json['attributes'] as List<dynamic>)
          .map((e) => EavValue.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ProductToJson(_$_Product instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'descriptionShort': instance.descriptionShort,
      'coverId': instance.coverId,
      'price': instance.price,
      'attributes': instance.attributes,
    };
