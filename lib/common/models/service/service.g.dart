// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Service _$$_ServiceFromJson(Map<String, dynamic> json) => _$_Service(
      id: json['id'] as String,
      label: json['label'] as String,
      descriptionShort: json['descriptionShort'] as String?,
      coverId: json['coverId'] as String?,
    );

Map<String, dynamic> _$$_ServiceToJson(_$_Service instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'descriptionShort': instance.descriptionShort,
      'coverId': instance.coverId,
    };
