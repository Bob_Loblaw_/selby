import 'package:freezed_annotation/freezed_annotation.dart';

part 'eav_value.freezed.dart';
part 'eav_value.g.dart';

@freezed
class EavValue with _$EavValue {
  factory EavValue.number({
    required String id,
    required String key,
    required num? value,
  }) = EavValueNumber;

  factory EavValue.string({
    required String id,
    required String key,
    required String? value,
  }) = EavValueString;

  factory EavValue.stringEnum({
    required String id,
    required String key,
    required String? value,
    required List<String> presetValues,
  }) = EavValueStringEnum;

  factory EavValue.stringMultiEnum({
    required String id,
    required String key,
    required List<String>? value,
    required List<String> presetValues,
  }) = EavValueStringMultiEnum;

  factory EavValue.date({
    required String id,
    required String key,
    required ({int year, int month, int day})? value,
  }) = EavValueDate;

  factory EavValue.dateTime({
    required String id,
    required String key,
    required DateTime? value,
  }) = EavValueDateTime;

  factory EavValue.fromJson(Map<String, dynamic> json) =>
      _$EavValueFromJson(json);
}

enum EavValueType {
  string,
  @JsonValue('string:enum')
  stringEnum,
  number,
  date,
  @JsonValue('date-time')
  dateTime,
  bool
}
