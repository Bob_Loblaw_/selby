// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'eav_value.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

EavValue _$EavValueFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'number':
      return EavValueNumber.fromJson(json);
    case 'string':
      return EavValueString.fromJson(json);
    case 'stringEnum':
      return EavValueStringEnum.fromJson(json);
    case 'stringMultiEnum':
      return EavValueStringMultiEnum.fromJson(json);
    case 'date':
      return EavValueDate.fromJson(json);
    case 'dateTime':
      return EavValueDateTime.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'EavValue',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
mixin _$EavValue {
  String get id => throw _privateConstructorUsedError;
  String get key => throw _privateConstructorUsedError;
  Object? get value => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EavValueCopyWith<EavValue> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EavValueCopyWith<$Res> {
  factory $EavValueCopyWith(EavValue value, $Res Function(EavValue) then) =
      _$EavValueCopyWithImpl<$Res, EavValue>;
  @useResult
  $Res call({String id, String key});
}

/// @nodoc
class _$EavValueCopyWithImpl<$Res, $Val extends EavValue>
    implements $EavValueCopyWith<$Res> {
  _$EavValueCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$EavValueNumberCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueNumberCopyWith(
          _$EavValueNumber value, $Res Function(_$EavValueNumber) then) =
      __$$EavValueNumberCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String key, num? value});
}

/// @nodoc
class __$$EavValueNumberCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueNumber>
    implements _$$EavValueNumberCopyWith<$Res> {
  __$$EavValueNumberCopyWithImpl(
      _$EavValueNumber _value, $Res Function(_$EavValueNumber) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
  }) {
    return _then(_$EavValueNumber(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueNumber implements EavValueNumber {
  _$EavValueNumber(
      {required this.id,
      required this.key,
      required this.value,
      final String? $type})
      : $type = $type ?? 'number';

  factory _$EavValueNumber.fromJson(Map<String, dynamic> json) =>
      _$$EavValueNumberFromJson(json);

  @override
  final String id;
  @override
  final String key;
  @override
  final num? value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.number(id: $id, key: $key, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueNumber &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, key, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueNumberCopyWith<_$EavValueNumber> get copyWith =>
      __$$EavValueNumberCopyWithImpl<_$EavValueNumber>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return number(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return number?.call(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (number != null) {
      return number(id, key, value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return number(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return number?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (number != null) {
      return number(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueNumberToJson(
      this,
    );
  }
}

abstract class EavValueNumber implements EavValue {
  factory EavValueNumber(
      {required final String id,
      required final String key,
      required final num? value}) = _$EavValueNumber;

  factory EavValueNumber.fromJson(Map<String, dynamic> json) =
      _$EavValueNumber.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  num? get value;
  @override
  @JsonKey(ignore: true)
  _$$EavValueNumberCopyWith<_$EavValueNumber> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EavValueStringCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueStringCopyWith(
          _$EavValueString value, $Res Function(_$EavValueString) then) =
      __$$EavValueStringCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String key, String? value});
}

/// @nodoc
class __$$EavValueStringCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueString>
    implements _$$EavValueStringCopyWith<$Res> {
  __$$EavValueStringCopyWithImpl(
      _$EavValueString _value, $Res Function(_$EavValueString) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
  }) {
    return _then(_$EavValueString(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueString implements EavValueString {
  _$EavValueString(
      {required this.id,
      required this.key,
      required this.value,
      final String? $type})
      : $type = $type ?? 'string';

  factory _$EavValueString.fromJson(Map<String, dynamic> json) =>
      _$$EavValueStringFromJson(json);

  @override
  final String id;
  @override
  final String key;
  @override
  final String? value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.string(id: $id, key: $key, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueString &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, key, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueStringCopyWith<_$EavValueString> get copyWith =>
      __$$EavValueStringCopyWithImpl<_$EavValueString>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return string(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return string?.call(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (string != null) {
      return string(id, key, value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return string(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return string?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (string != null) {
      return string(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueStringToJson(
      this,
    );
  }
}

abstract class EavValueString implements EavValue {
  factory EavValueString(
      {required final String id,
      required final String key,
      required final String? value}) = _$EavValueString;

  factory EavValueString.fromJson(Map<String, dynamic> json) =
      _$EavValueString.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  String? get value;
  @override
  @JsonKey(ignore: true)
  _$$EavValueStringCopyWith<_$EavValueString> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EavValueStringEnumCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueStringEnumCopyWith(_$EavValueStringEnum value,
          $Res Function(_$EavValueStringEnum) then) =
      __$$EavValueStringEnumCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String key, String? value, List<String> presetValues});
}

/// @nodoc
class __$$EavValueStringEnumCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueStringEnum>
    implements _$$EavValueStringEnumCopyWith<$Res> {
  __$$EavValueStringEnumCopyWithImpl(
      _$EavValueStringEnum _value, $Res Function(_$EavValueStringEnum) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
    Object? presetValues = null,
  }) {
    return _then(_$EavValueStringEnum(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String?,
      presetValues: null == presetValues
          ? _value._presetValues
          : presetValues // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueStringEnum implements EavValueStringEnum {
  _$EavValueStringEnum(
      {required this.id,
      required this.key,
      required this.value,
      required final List<String> presetValues,
      final String? $type})
      : _presetValues = presetValues,
        $type = $type ?? 'stringEnum';

  factory _$EavValueStringEnum.fromJson(Map<String, dynamic> json) =>
      _$$EavValueStringEnumFromJson(json);

  @override
  final String id;
  @override
  final String key;
  @override
  final String? value;
  final List<String> _presetValues;
  @override
  List<String> get presetValues {
    if (_presetValues is EqualUnmodifiableListView) return _presetValues;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_presetValues);
  }

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.stringEnum(id: $id, key: $key, value: $value, presetValues: $presetValues)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueStringEnum &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.value, value) || other.value == value) &&
            const DeepCollectionEquality()
                .equals(other._presetValues, _presetValues));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, key, value,
      const DeepCollectionEquality().hash(_presetValues));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueStringEnumCopyWith<_$EavValueStringEnum> get copyWith =>
      __$$EavValueStringEnumCopyWithImpl<_$EavValueStringEnum>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return stringEnum(id, key, value, presetValues);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return stringEnum?.call(id, key, value, presetValues);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (stringEnum != null) {
      return stringEnum(id, key, value, presetValues);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return stringEnum(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return stringEnum?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (stringEnum != null) {
      return stringEnum(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueStringEnumToJson(
      this,
    );
  }
}

abstract class EavValueStringEnum implements EavValue {
  factory EavValueStringEnum(
      {required final String id,
      required final String key,
      required final String? value,
      required final List<String> presetValues}) = _$EavValueStringEnum;

  factory EavValueStringEnum.fromJson(Map<String, dynamic> json) =
      _$EavValueStringEnum.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  String? get value;
  List<String> get presetValues;
  @override
  @JsonKey(ignore: true)
  _$$EavValueStringEnumCopyWith<_$EavValueStringEnum> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EavValueStringMultiEnumCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueStringMultiEnumCopyWith(_$EavValueStringMultiEnum value,
          $Res Function(_$EavValueStringMultiEnum) then) =
      __$$EavValueStringMultiEnumCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id, String key, List<String>? value, List<String> presetValues});
}

/// @nodoc
class __$$EavValueStringMultiEnumCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueStringMultiEnum>
    implements _$$EavValueStringMultiEnumCopyWith<$Res> {
  __$$EavValueStringMultiEnumCopyWithImpl(_$EavValueStringMultiEnum _value,
      $Res Function(_$EavValueStringMultiEnum) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
    Object? presetValues = null,
  }) {
    return _then(_$EavValueStringMultiEnum(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value._value
          : value // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      presetValues: null == presetValues
          ? _value._presetValues
          : presetValues // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueStringMultiEnum implements EavValueStringMultiEnum {
  _$EavValueStringMultiEnum(
      {required this.id,
      required this.key,
      required final List<String>? value,
      required final List<String> presetValues,
      final String? $type})
      : _value = value,
        _presetValues = presetValues,
        $type = $type ?? 'stringMultiEnum';

  factory _$EavValueStringMultiEnum.fromJson(Map<String, dynamic> json) =>
      _$$EavValueStringMultiEnumFromJson(json);

  @override
  final String id;
  @override
  final String key;
  final List<String>? _value;
  @override
  List<String>? get value {
    final value = _value;
    if (value == null) return null;
    if (_value is EqualUnmodifiableListView) return _value;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String> _presetValues;
  @override
  List<String> get presetValues {
    if (_presetValues is EqualUnmodifiableListView) return _presetValues;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_presetValues);
  }

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.stringMultiEnum(id: $id, key: $key, value: $value, presetValues: $presetValues)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueStringMultiEnum &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            const DeepCollectionEquality().equals(other._value, _value) &&
            const DeepCollectionEquality()
                .equals(other._presetValues, _presetValues));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      key,
      const DeepCollectionEquality().hash(_value),
      const DeepCollectionEquality().hash(_presetValues));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueStringMultiEnumCopyWith<_$EavValueStringMultiEnum> get copyWith =>
      __$$EavValueStringMultiEnumCopyWithImpl<_$EavValueStringMultiEnum>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return stringMultiEnum(id, key, value, presetValues);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return stringMultiEnum?.call(id, key, value, presetValues);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (stringMultiEnum != null) {
      return stringMultiEnum(id, key, value, presetValues);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return stringMultiEnum(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return stringMultiEnum?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (stringMultiEnum != null) {
      return stringMultiEnum(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueStringMultiEnumToJson(
      this,
    );
  }
}

abstract class EavValueStringMultiEnum implements EavValue {
  factory EavValueStringMultiEnum(
      {required final String id,
      required final String key,
      required final List<String>? value,
      required final List<String> presetValues}) = _$EavValueStringMultiEnum;

  factory EavValueStringMultiEnum.fromJson(Map<String, dynamic> json) =
      _$EavValueStringMultiEnum.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  List<String>? get value;
  List<String> get presetValues;
  @override
  @JsonKey(ignore: true)
  _$$EavValueStringMultiEnumCopyWith<_$EavValueStringMultiEnum> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EavValueDateCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueDateCopyWith(
          _$EavValueDate value, $Res Function(_$EavValueDate) then) =
      __$$EavValueDateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String key, ({int day, int month, int year})? value});
}

/// @nodoc
class __$$EavValueDateCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueDate>
    implements _$$EavValueDateCopyWith<$Res> {
  __$$EavValueDateCopyWithImpl(
      _$EavValueDate _value, $Res Function(_$EavValueDate) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
  }) {
    return _then(_$EavValueDate(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as ({int day, int month, int year})?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueDate implements EavValueDate {
  _$EavValueDate(
      {required this.id,
      required this.key,
      required this.value,
      final String? $type})
      : $type = $type ?? 'date';

  factory _$EavValueDate.fromJson(Map<String, dynamic> json) =>
      _$$EavValueDateFromJson(json);

  @override
  final String id;
  @override
  final String key;
  @override
  final ({int day, int month, int year})? value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.date(id: $id, key: $key, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueDate &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, key, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueDateCopyWith<_$EavValueDate> get copyWith =>
      __$$EavValueDateCopyWithImpl<_$EavValueDate>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return date(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return date?.call(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (date != null) {
      return date(id, key, value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return date(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return date?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (date != null) {
      return date(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueDateToJson(
      this,
    );
  }
}

abstract class EavValueDate implements EavValue {
  factory EavValueDate(
      {required final String id,
      required final String key,
      required final ({int day, int month, int year})? value}) = _$EavValueDate;

  factory EavValueDate.fromJson(Map<String, dynamic> json) =
      _$EavValueDate.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  ({int day, int month, int year})? get value;
  @override
  @JsonKey(ignore: true)
  _$$EavValueDateCopyWith<_$EavValueDate> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EavValueDateTimeCopyWith<$Res>
    implements $EavValueCopyWith<$Res> {
  factory _$$EavValueDateTimeCopyWith(
          _$EavValueDateTime value, $Res Function(_$EavValueDateTime) then) =
      __$$EavValueDateTimeCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String key, DateTime? value});
}

/// @nodoc
class __$$EavValueDateTimeCopyWithImpl<$Res>
    extends _$EavValueCopyWithImpl<$Res, _$EavValueDateTime>
    implements _$$EavValueDateTimeCopyWith<$Res> {
  __$$EavValueDateTimeCopyWithImpl(
      _$EavValueDateTime _value, $Res Function(_$EavValueDateTime) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? value = freezed,
  }) {
    return _then(_$EavValueDateTime(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: freezed == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$EavValueDateTime implements EavValueDateTime {
  _$EavValueDateTime(
      {required this.id,
      required this.key,
      required this.value,
      final String? $type})
      : $type = $type ?? 'dateTime';

  factory _$EavValueDateTime.fromJson(Map<String, dynamic> json) =>
      _$$EavValueDateTimeFromJson(json);

  @override
  final String id;
  @override
  final String key;
  @override
  final DateTime? value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'EavValue.dateTime(id: $id, key: $key, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EavValueDateTime &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.value, value) || other.value == value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, key, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$EavValueDateTimeCopyWith<_$EavValueDateTime> get copyWith =>
      __$$EavValueDateTimeCopyWithImpl<_$EavValueDateTime>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String id, String key, num? value) number,
    required TResult Function(String id, String key, String? value) string,
    required TResult Function(
            String id, String key, String? value, List<String> presetValues)
        stringEnum,
    required TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)
        stringMultiEnum,
    required TResult Function(
            String id, String key, ({int day, int month, int year})? value)
        date,
    required TResult Function(String id, String key, DateTime? value) dateTime,
  }) {
    return dateTime(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String id, String key, num? value)? number,
    TResult? Function(String id, String key, String? value)? string,
    TResult? Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult? Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult? Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult? Function(String id, String key, DateTime? value)? dateTime,
  }) {
    return dateTime?.call(id, key, value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String id, String key, num? value)? number,
    TResult Function(String id, String key, String? value)? string,
    TResult Function(
            String id, String key, String? value, List<String> presetValues)?
        stringEnum,
    TResult Function(String id, String key, List<String>? value,
            List<String> presetValues)?
        stringMultiEnum,
    TResult Function(
            String id, String key, ({int day, int month, int year})? value)?
        date,
    TResult Function(String id, String key, DateTime? value)? dateTime,
    required TResult orElse(),
  }) {
    if (dateTime != null) {
      return dateTime(id, key, value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(EavValueNumber value) number,
    required TResult Function(EavValueString value) string,
    required TResult Function(EavValueStringEnum value) stringEnum,
    required TResult Function(EavValueStringMultiEnum value) stringMultiEnum,
    required TResult Function(EavValueDate value) date,
    required TResult Function(EavValueDateTime value) dateTime,
  }) {
    return dateTime(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(EavValueNumber value)? number,
    TResult? Function(EavValueString value)? string,
    TResult? Function(EavValueStringEnum value)? stringEnum,
    TResult? Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult? Function(EavValueDate value)? date,
    TResult? Function(EavValueDateTime value)? dateTime,
  }) {
    return dateTime?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(EavValueNumber value)? number,
    TResult Function(EavValueString value)? string,
    TResult Function(EavValueStringEnum value)? stringEnum,
    TResult Function(EavValueStringMultiEnum value)? stringMultiEnum,
    TResult Function(EavValueDate value)? date,
    TResult Function(EavValueDateTime value)? dateTime,
    required TResult orElse(),
  }) {
    if (dateTime != null) {
      return dateTime(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$EavValueDateTimeToJson(
      this,
    );
  }
}

abstract class EavValueDateTime implements EavValue {
  factory EavValueDateTime(
      {required final String id,
      required final String key,
      required final DateTime? value}) = _$EavValueDateTime;

  factory EavValueDateTime.fromJson(Map<String, dynamic> json) =
      _$EavValueDateTime.fromJson;

  @override
  String get id;
  @override
  String get key;
  @override
  DateTime? get value;
  @override
  @JsonKey(ignore: true)
  _$$EavValueDateTimeCopyWith<_$EavValueDateTime> get copyWith =>
      throw _privateConstructorUsedError;
}
