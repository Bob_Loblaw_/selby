// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eav_value.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$EavValueNumber _$$EavValueNumberFromJson(Map<String, dynamic> json) =>
    _$EavValueNumber(
      id: json['id'] as String,
      key: json['key'] as String,
      value: json['value'] as num?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueNumberToJson(_$EavValueNumber instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$EavValueString _$$EavValueStringFromJson(Map<String, dynamic> json) =>
    _$EavValueString(
      id: json['id'] as String,
      key: json['key'] as String,
      value: json['value'] as String?,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueStringToJson(_$EavValueString instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$EavValueStringEnum _$$EavValueStringEnumFromJson(Map<String, dynamic> json) =>
    _$EavValueStringEnum(
      id: json['id'] as String,
      key: json['key'] as String,
      value: json['value'] as String?,
      presetValues: (json['presetValues'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueStringEnumToJson(
        _$EavValueStringEnum instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value,
      'presetValues': instance.presetValues,
      'runtimeType': instance.$type,
    };

_$EavValueStringMultiEnum _$$EavValueStringMultiEnumFromJson(
        Map<String, dynamic> json) =>
    _$EavValueStringMultiEnum(
      id: json['id'] as String,
      key: json['key'] as String,
      value:
          (json['value'] as List<dynamic>?)?.map((e) => e as String).toList(),
      presetValues: (json['presetValues'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueStringMultiEnumToJson(
        _$EavValueStringMultiEnum instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value,
      'presetValues': instance.presetValues,
      'runtimeType': instance.$type,
    };

_$EavValueDate _$$EavValueDateFromJson(Map<String, dynamic> json) =>
    _$EavValueDate(
      id: json['id'] as String,
      key: json['key'] as String,
      value: _$recordConvertNullable(
        json['value'],
        ($jsonValue) => (
          day: $jsonValue['day'] as int,
          month: $jsonValue['month'] as int,
          year: $jsonValue['year'] as int,
        ),
      ),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueDateToJson(_$EavValueDate instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value == null
          ? null
          : {
              'day': instance.value!.day,
              'month': instance.value!.month,
              'year': instance.value!.year,
            },
      'runtimeType': instance.$type,
    };

$Rec? _$recordConvertNullable<$Rec>(
  Object? value,
  $Rec Function(Map) convert,
) =>
    value == null ? null : convert(value as Map<String, dynamic>);

_$EavValueDateTime _$$EavValueDateTimeFromJson(Map<String, dynamic> json) =>
    _$EavValueDateTime(
      id: json['id'] as String,
      key: json['key'] as String,
      value: json['value'] == null
          ? null
          : DateTime.parse(json['value'] as String),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$EavValueDateTimeToJson(_$EavValueDateTime instance) =>
    <String, dynamic>{
      'id': instance.id,
      'key': instance.key,
      'value': instance.value?.toIso8601String(),
      'runtimeType': instance.$type,
    };
