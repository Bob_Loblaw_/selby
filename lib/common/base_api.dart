import 'package:selby/common/common.dart';
import 'package:selby/utils/utils.dart';
import 'package:dio/dio.dart';

/// Base class for objects that communicate with remote data sources.
abstract class Api {
  const Api();
}

abstract class RestfulApi extends Api {
  const RestfulApi();

  Future<Result<T>> run<T>(Future<Result<T>> Function() fn) async {
    try {
      return await fn.call();
    } on RefreshTokenError catch (_) {
      return Result.failure('Пожалуйста, перезайдите в приложение');
    } on DioException catch (e) {
      final sc = e.response?.statusCode;
      final msg = e.response?.data['message'];
      return Result.failure(
        msg ??
            (sc == 401
                ? 'У вас недостаточно прав для просмотра этого контента'
                : 'Что-то пошло не так!'),
      );
    } catch (e) {
      return Result.failure(e.toString());
    }
  }
}
