/// Interface for objects that provide data storage and access
abstract class IRepository<T> {
  String getElementId(T element);

  Future<T> getOne(String id);

  Future<List<T>> paginate(int page, {Map<String, String>? params});

  Future<void> saveOne(T element);

  Future<void> saveCollection(List<T> elements);

  Future<void> deleteOne(String id);

  Future<void> deleteCollection(List<String> ids);
}
