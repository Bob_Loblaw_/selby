import 'package:selby/common/common.dart';
import 'package:selby/utils/utils.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:hydrated_bloc/hydrated_bloc.dart';

part 'state.dart';
part 'cubit.freezed.dart';
part 'cubit.g.dart';
part 'pagination.dart';

/// Default page on which we start pagination. Subject to rules of backend.
/// Historically was '1' in most projects.
const kDefaultPaginationStartingPage = 1;

/// Base pagination cubit that expects api to return [Pagination].
/// <T> should be a concrete pagination object (maybe [User], [Article], [Order], ...).
///
/// Example usage:
///
/// ```dart
///  class UserPaginationCubit extends PaginationCubit<User> {
///    final UserApi api; //or use getIt
///
///    @override
///    Future<Result<Pagination<User>>> apiCall(int page) {
///      final api = this.api; // or use getIt;
///      return api.getUsers(page: page, limit: state.limit);
///    }
///  }
/// ```
///
/// Usage:
///
/// ``` dart
/// BlocConsumer<UserPaginationCubit, PaginationState<User>>(
///   listener: (ctx, state) {
///     if (state.lastError != null) {
///       // show error to user
///     }
///   },
///   builder: (ctx, state) {
///     return ...
///   },
/// ),
/// ```
abstract class PaginationCubit<T> extends Cubit<PaginationState<T>> {
  final int startingPage;

  PaginationCubit({
    this.startingPage = kDefaultPaginationStartingPage,
    required int limit,
  }) : super(PaginationState(
          status: PaginationStatus.init,
          page: startingPage,
          limit: limit,
          total: -1,
          items: const [],
          canLoadMore: false,
          lastError: '',
        ));

  @override
  void emit(state) {
    if (isClosed) return;
    super.emit(state);
  }

  Future<Result<Pagination<T>>> apiCall(int page);

  Future<void> _load({bool fromStart = false}) async {
    if (state.status.isLoading) return;
    emit(state.copyWith(
      status: PaginationStatus.loading,
      page: fromStart ? startingPage : state.page,
      items: fromStart ? <T>[] : state.items,
      lastError: null,
    ) as PaginationState<T>);

    final res = await apiCall(state.page);
    if (res.success) {
      emit(state.copyWith(
        page: state.page + 1,
        status: PaginationStatus.success,
        total: res.data.total,
        items: fromStart
            ? List<T>.from(res.data.data)
            : (List<T>.from(state.items)..addAll(res.data.data)),
        canLoadMore: res.data.limit == res.data.data.length,
        lastError: null,
      ) as PaginationState<T>);
    } else {
      emit(state.copyWith(
        status: PaginationStatus.failure,
        lastError: res.error,
      ) as PaginationState<T>);
    }
  }

  void load() {
    _load();
  }

  Future<void> loadAwaited() {
    return _load();
  }

  void loadFromStart() {
    _load(fromStart: true);
  }

  Future<void> loadFromStartAwaited() {
    return _load(fromStart: true);
  }
}

abstract class ConnectionAwarePaginationCubit<T> extends PaginationCubit<T> {
  final String noInternetErrorMessage;

  ConnectionAwarePaginationCubit({
    int startingPage = kDefaultPaginationStartingPage,
    required int limit,
    required this.noInternetErrorMessage,
  }) : super(limit: limit, startingPage: startingPage);

  @override
  Future<void> _load({bool fromStart = false}) {
    if (state.status.isLoading) return Future.value(null);
    final conn = Connection.status.availableForPhones;
    if (!conn) {
      if (!(fromStart && state.items.isNotEmpty)) {
        emit(state.copyWith(
          status: PaginationStatus.failure,
          lastError: noInternetErrorMessage,
        ) as PaginationState<T>);
      }
      return Future.value(null);
    } else {
      return super._load(fromStart: fromStart);
    }
  }
}

abstract class ConnectionAwareHydratedPaginationCubit<T>
    extends ConnectionAwarePaginationCubit<T> with HydratedMixin {
  final T Function(dynamic) fromJsonMapper;
  final dynamic Function(T) toJsonMapper;

  ConnectionAwareHydratedPaginationCubit({
    int startingPage = kDefaultPaginationStartingPage,
    required int limit,
    required String noInternetErrorMessage,
    required this.fromJsonMapper,
    required this.toJsonMapper,
  }) : super(
          limit: limit,
          startingPage: startingPage,
          noInternetErrorMessage: noInternetErrorMessage,
        );

  @override
  PaginationState<T>? fromJson(Map<String, dynamic> json) {
    return PaginationState<T>.fromJson(json, this.fromJsonMapper);
  }

  @override
  Map<String, dynamic>? toJson(PaginationState<T> state) {
    return state.toJson((t) => toJsonMapper(t as T)); // type casts are weird
  }
}
