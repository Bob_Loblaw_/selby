// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PaginationState<T> _$PaginationStateFromJson<T>(
    Map<String, dynamic> json, T Function(Object?) fromJsonT) {
  return _PaginationState<T>.fromJson(json, fromJsonT);
}

/// @nodoc
mixin _$PaginationState<T> {
  PaginationStatus get status => throw _privateConstructorUsedError;
  int get page => throw _privateConstructorUsedError;
  int get limit => throw _privateConstructorUsedError;
  int get total => throw _privateConstructorUsedError;
  List<T> get items => throw _privateConstructorUsedError;
  bool get canLoadMore => throw _privateConstructorUsedError;
  String? get lastError => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson(Object? Function(T) toJsonT) =>
      throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaginationStateCopyWith<T, PaginationState<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaginationStateCopyWith<T, $Res> {
  factory $PaginationStateCopyWith(
          PaginationState<T> value, $Res Function(PaginationState<T>) then) =
      _$PaginationStateCopyWithImpl<T, $Res, PaginationState<T>>;
  @useResult
  $Res call(
      {PaginationStatus status,
      int page,
      int limit,
      int total,
      List<T> items,
      bool canLoadMore,
      String? lastError});
}

/// @nodoc
class _$PaginationStateCopyWithImpl<T, $Res, $Val extends PaginationState<T>>
    implements $PaginationStateCopyWith<T, $Res> {
  _$PaginationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? page = null,
    Object? limit = null,
    Object? total = null,
    Object? items = null,
    Object? canLoadMore = null,
    Object? lastError = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PaginationStatus,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int,
      items: null == items
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<T>,
      canLoadMore: null == canLoadMore
          ? _value.canLoadMore
          : canLoadMore // ignore: cast_nullable_to_non_nullable
              as bool,
      lastError: freezed == lastError
          ? _value.lastError
          : lastError // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PaginationStateCopyWith<T, $Res>
    implements $PaginationStateCopyWith<T, $Res> {
  factory _$$_PaginationStateCopyWith(_$_PaginationState<T> value,
          $Res Function(_$_PaginationState<T>) then) =
      __$$_PaginationStateCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call(
      {PaginationStatus status,
      int page,
      int limit,
      int total,
      List<T> items,
      bool canLoadMore,
      String? lastError});
}

/// @nodoc
class __$$_PaginationStateCopyWithImpl<T, $Res>
    extends _$PaginationStateCopyWithImpl<T, $Res, _$_PaginationState<T>>
    implements _$$_PaginationStateCopyWith<T, $Res> {
  __$$_PaginationStateCopyWithImpl(
      _$_PaginationState<T> _value, $Res Function(_$_PaginationState<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? page = null,
    Object? limit = null,
    Object? total = null,
    Object? items = null,
    Object? canLoadMore = null,
    Object? lastError = freezed,
  }) {
    return _then(_$_PaginationState<T>(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as PaginationStatus,
      page: null == page
          ? _value.page
          : page // ignore: cast_nullable_to_non_nullable
              as int,
      limit: null == limit
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int,
      total: null == total
          ? _value.total
          : total // ignore: cast_nullable_to_non_nullable
              as int,
      items: null == items
          ? _value._items
          : items // ignore: cast_nullable_to_non_nullable
              as List<T>,
      canLoadMore: null == canLoadMore
          ? _value.canLoadMore
          : canLoadMore // ignore: cast_nullable_to_non_nullable
              as bool,
      lastError: freezed == lastError
          ? _value.lastError
          : lastError // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable(genericArgumentFactories: true)
class _$_PaginationState<T> implements _PaginationState<T> {
  const _$_PaginationState(
      {required this.status,
      required this.page,
      required this.limit,
      required this.total,
      required final List<T> items,
      required this.canLoadMore,
      required this.lastError})
      : _items = items;

  factory _$_PaginationState.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =>
      _$$_PaginationStateFromJson(json, fromJsonT);

  @override
  final PaginationStatus status;
  @override
  final int page;
  @override
  final int limit;
  @override
  final int total;
  final List<T> _items;
  @override
  List<T> get items {
    if (_items is EqualUnmodifiableListView) return _items;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_items);
  }

  @override
  final bool canLoadMore;
  @override
  final String? lastError;

  @override
  String toString() {
    return 'PaginationState<$T>(status: $status, page: $page, limit: $limit, total: $total, items: $items, canLoadMore: $canLoadMore, lastError: $lastError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PaginationState<T> &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.page, page) || other.page == page) &&
            (identical(other.limit, limit) || other.limit == limit) &&
            (identical(other.total, total) || other.total == total) &&
            const DeepCollectionEquality().equals(other._items, _items) &&
            (identical(other.canLoadMore, canLoadMore) ||
                other.canLoadMore == canLoadMore) &&
            (identical(other.lastError, lastError) ||
                other.lastError == lastError));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, status, page, limit, total,
      const DeepCollectionEquality().hash(_items), canLoadMore, lastError);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PaginationStateCopyWith<T, _$_PaginationState<T>> get copyWith =>
      __$$_PaginationStateCopyWithImpl<T, _$_PaginationState<T>>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson(Object? Function(T) toJsonT) {
    return _$$_PaginationStateToJson<T>(this, toJsonT);
  }
}

abstract class _PaginationState<T> implements PaginationState<T> {
  const factory _PaginationState(
      {required final PaginationStatus status,
      required final int page,
      required final int limit,
      required final int total,
      required final List<T> items,
      required final bool canLoadMore,
      required final String? lastError}) = _$_PaginationState<T>;

  factory _PaginationState.fromJson(
          Map<String, dynamic> json, T Function(Object?) fromJsonT) =
      _$_PaginationState<T>.fromJson;

  @override
  PaginationStatus get status;
  @override
  int get page;
  @override
  int get limit;
  @override
  int get total;
  @override
  List<T> get items;
  @override
  bool get canLoadMore;
  @override
  String? get lastError;
  @override
  @JsonKey(ignore: true)
  _$$_PaginationStateCopyWith<T, _$_PaginationState<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
