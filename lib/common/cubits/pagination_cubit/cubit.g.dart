// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cubit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PaginationState<T> _$$_PaginationStateFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    _$_PaginationState<T>(
      status: $enumDecode(_$PaginationStatusEnumMap, json['status']),
      page: json['page'] as int,
      limit: json['limit'] as int,
      total: json['total'] as int,
      items: (json['items'] as List<dynamic>).map(fromJsonT).toList(),
      canLoadMore: json['canLoadMore'] as bool,
      lastError: json['lastError'] as String?,
    );

Map<String, dynamic> _$$_PaginationStateToJson<T>(
  _$_PaginationState<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'status': _$PaginationStatusEnumMap[instance.status]!,
      'page': instance.page,
      'limit': instance.limit,
      'total': instance.total,
      'items': instance.items.map(toJsonT).toList(),
      'canLoadMore': instance.canLoadMore,
      'lastError': instance.lastError,
    };

const _$PaginationStatusEnumMap = {
  PaginationStatus.init: 'init',
  PaginationStatus.loading: 'loading',
  PaginationStatus.failure: 'failure',
  PaginationStatus.success: 'success',
};
