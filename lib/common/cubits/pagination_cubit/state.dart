part of 'cubit.dart';

@Freezed(genericArgumentFactories: true)
class PaginationState<T> with _$PaginationState {
  const factory PaginationState({
    required PaginationStatus status,
    required int page,
    required int limit,
    required int total,
    required List<T> items,
    required bool canLoadMore,
    required String? lastError,
  }) = _PaginationState;

  factory PaginationState.fromJson(
          Map<String, dynamic> json, T Function(Object?) mapper) =>
      _$PaginationStateFromJson(json, mapper);
}

enum PaginationStatus { init, loading, failure, success }

extension PaginationStatusX on PaginationStatus {
  bool get isInitial => this == PaginationStatus.init;
  bool get isLoading => this == PaginationStatus.loading;
  bool get isFailure => this == PaginationStatus.failure;
  bool get isSuccess => this == PaginationStatus.success;
}
