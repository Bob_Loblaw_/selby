part of 'cubit.dart';

/// [PaginationCubit] expects api to provide pagination in this format.

class Pagination<T> {
  final int limit;
  final int page;
  final int total;
  final List<T> data;

  Pagination({
    required this.limit,
    required this.page,
    required this.total,
    required this.data,
  });

  factory Pagination.fromJson(
    Map<String, dynamic> json, {
    required T Function(Map<String, dynamic> unit) mapper,
  }) {
    return Pagination(
      limit: json['limit'],
      page: json['page'],
      total: json['total'],
      data:
          (json['data'] as List<Map<String, dynamic>>).map<T>(mapper).toList(),
    );
  }
}
