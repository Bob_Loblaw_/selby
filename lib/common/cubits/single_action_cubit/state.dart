part of 'cubit.dart';

abstract class SingleActionState<T> {
  bool get isInitial => this is SAInitial;
  bool get isLoading => this is SALoading;
  bool get isSuccess => this is SASuccess;
  bool get isFailure => this is SAFailure;

  T get result {
    assert(this is SASuccess<T>);
    return (this as SASuccess<T>)._res;
  }

  String get error {
    assert(this is SAFailure);
    return (this as SAFailure).description;
  }
}

class SAInitial<T> extends SingleActionState<T> {}

class SALoading<T> extends SingleActionState<T> {}

class SASuccess<T> extends SingleActionState<T> {
  final T _res;

  SASuccess(this._res);
}

class SAFailure<T> extends SingleActionState<T> {
  final String description;

  SAFailure(this.description);
}
