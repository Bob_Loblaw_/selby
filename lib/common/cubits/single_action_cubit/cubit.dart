import 'package:selby/common/common.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'state.dart';

/// Base class for cubits that do one thing (action)
/// and report the result of this thing (action).
///
/// Extend this class and override the [action] method
/// to get a quick cubit impl for single-use actions:
///
/// ```dart
/// class LoginCubit extends SingleActionCubit<TokenPair> {
///   final LoginApi api; // or use getIt
///
///   @override
///   Future<Result<TokenPair>> action([String? login, String? password]) {
///     final api = this.api; // or use getIt
///     return api.logIn(login!, password!);
///   }
/// }
/// ```
///
/// Create an instance of this class with [BlocProvider]:
///
/// ```dart
/// return BlocProvider<LoginCubit>(
///   create: (ctx) => LoginCubit({maybe api here, or use getIt}),
///   child: ...,
/// )
/// ```
///
/// Then, lower within this subtree
/// in [BlocBuilder] / [BlocListener] / [BlocConsumer]:
///
/// ```dart
/// BlocConsumer<LoginCubit, SingleActionState<TokenPair>>(
///   listener: (ctx, state) {
///     if (state is SAFailure) {
///       // show message to user
///     } else if (state is SASuccess) {
///       final res = state.result as TokenPair;
///       Storage.setTokenPair(res).then(
///         (_) => Navigator.pushReplacementNamed("mainScreen");
///       );
///     }
///   },
///   builder: (ctx, state) {
///     return ...;
///   }
/// ),
/// ```
///
/// To trigger [action], just set up a call for this method in button [onTap]
/// or something like that:
///
/// ```dart
/// ElevatedButton(
///   onPressed: () {
///     final login = loginController.text;
///     final password = passController.text.trim();
///     BlocProvider.of<LoginCubit>(context).login(login, password);
///   },
///   child: Text('Log In'),
/// ),
/// ```
///

abstract class SingleActionCubit<Output>
    extends Cubit<SingleActionState<Output>> {
  SingleActionCubit() : super(SAInitial());
}

abstract class SingleActionCubitNoArg<Output>
    extends SingleActionCubit<Output> {
  Future<Result<Output>> action();

  void load() async {
    if (state.isLoading) return;
    emit(SALoading());
    final res = await action();
    if (res.success) {
      emit(SASuccess<Output>(res.data));
    } else {
      emit(SAFailure(res.error));
    }
  }
}

/// [SingleActionCubit] that expects user
/// to supply a custom class to work with.
abstract class SingleActionCubitWithArg<Arg, Output>
    extends SingleActionCubit<Output> {
  Future<Result<Output>> action(Arg arg);

  void load(Arg param) async {
    if (state.isLoading) return;
    emit(SALoading());
    final res = await action(param);
    if (res.success) {
      emit(SASuccess<Output>(res.data));
    } else {
      emit(SAFailure(res.error));
    }
  }
}

/// [SingleActionCubit] with a [Record] for a param.
abstract class SingleActionCubitWithRecordParam<Output>
    extends SingleActionCubit<Output> {
  Future<Result<Output>> action(Record param);

  void load(Record param) async {
    if (state.isLoading) return;
    emit(SALoading());
    final res = await action(param);
    if (res.success) {
      emit(SASuccess<Output>(res.data));
    } else {
      emit(SAFailure(res.error));
    }
  }
}
