part of 'main.dart';

final GlobalKey<NavigatorState> appNavigatorKey = GlobalKey<NavigatorState>();

class Selby extends StatelessWidget {
  Selby({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Notifications(
      child: MaterialApp.router(
        routerConfig: goRouter,
        theme: getAppTheme(),
        builder: (ctx, child) {
          final mq = MediaQuery.of(context);
          return MediaQuery(
            data: mq.copyWith(textScaleFactor: 1.0),
            child: child!,
          );
        },
      ),
    );
  }
}
