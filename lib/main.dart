import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:go_router/go_router.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart' hide Storage;
import 'package:path_provider/path_provider.dart';
import 'package:selby/screens/screens.dart';

import 'common/models/models.dart';
import 'utils/utils.dart';

part 'application.dart';
part 'router.dart';

void main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  Paint.enableDithering = true;
  //HttpOverrides.global = MyHttpOverrides();

  await Future.wait([
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]),
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge),
    Storage.init(),
    Connection.setup(),
  ]);
  Tokens.initTokens();
  getItSetup();
  setDarkOverlayStyle();

  /*await Firebase.initializeApp();
  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
  };

  runZonedGuarded(run, (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack);
  });*/

  run();
}

void run() async {
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getTemporaryDirectory(),
  );

  runApp(Selby());
}
