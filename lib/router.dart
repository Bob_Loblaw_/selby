part of 'main.dart';

final _rootNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'root');

final goRouter = GoRouter(
  navigatorKey: _rootNavigatorKey,
  routes: [
    GoRoute(
      path: '/',
      pageBuilder: (ctx, state) {
        return _page(const SetupScreen());
      },
    ),
    StatefulShellRoute(
      builder: (ctx, state, shell) => shell,
      navigatorContainerBuilder: (ctx, shell, children) {
        return MainScreenShell(shell: shell, children: children);
      },
      branches: [
        StatefulShellBranch(routes: [
          GoRoute(
            path: HomeScreen.name,
            pageBuilder: (ctx, state) {
              return _page(const HomeScreen());
            },
          ),
          GoRoute(
            path: CategoriesScreen.name,
            pageBuilder: (ctx, state) {
              final l = (state.extra as List).map<Category>((e) {
                return _parseArg<Category>(e, Category.fromJson);
              }).toList();
              return _page(CategoriesScreen(breadcrumbs: l));
            },
          ),
          GoRoute(
            path: CategoryItemsScreen.name,
            pageBuilder: (ctx, state) {
              final l = (state.extra as List).map<Category>((e) {
                return _parseArg<Category>(e, Category.fromJson);
              }).toList();
              return _page(CategoryItemsScreen(breadcrumbs: l));
            },
          ),
          GoRoute(
            path: ProductViewScreen.name,
            pageBuilder: (ctx, state) => _page(
              ProductViewScreen(
                product: _parseArg<Product>(
                  state.extra,
                  Product.fromJson,
                ),
              ),
            ),
          ),
          GoRoute(
            path: FilterScreen.name,
            pageBuilder: (ctx, state) {
              return _page(const FilterScreen());
            },
            routes: [
              GoRoute(
                path: FilterMultichoiceShowAll.name,
                pageBuilder: (ctx, state) {
                  final map = state.extra as Map<String, dynamic>;
                  final String title = map['title'];
                  final List<Choice> choices = (map['choices'] as List)
                      .map((e) => _parseArg(e, Choice.fromJson))
                      .toList();

                  return _page(
                    FilterMultichoiceShowAll(
                      title: title,
                      choices: choices,
                    ),
                  );
                },
              ),
            ],
          ),
        ]),
        StatefulShellBranch(routes: [
          GoRoute(
            path: CartScreen.name,
            pageBuilder: (ctx, state) {
              return _page(const CartScreen());
            },
          ),
        ]),
        StatefulShellBranch(routes: [
          GoRoute(
            path: ProfileScreen.name,
            pageBuilder: (ctx, state) {
              return _page(const ProfileScreen());
            },
          ),
        ]),
      ],
    ),
  ],
);

// shell converts args to primitives when conserving state. Handle this case
T _parseArg<T>(
  dynamic value,
  T Function(Map<String, dynamic> json) fromJson,
) {
  if (value is Map<String, dynamic>) {
    return fromJson(value);
  } else {
    assert(value is T);
    return value as T;
  }
}

Page _page(Widget child) {
  return CustomTransitionPage(
    transitionsBuilder: (ctx, anim1, anim2, child) {
      return CupertinoPageTransition(
        primaryRouteAnimation: anim1,
        secondaryRouteAnimation: anim2,
        linearTransition: true,
        child: child,
      );
    },
    child: child,
  );
}
