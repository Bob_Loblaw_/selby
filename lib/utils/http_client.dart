import 'package:selby/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:jwt_decode/jwt_decode.dart';

const _kDefaultConnectTimeout = Duration(seconds: 5);

/// Uses Dio. Does token refresh
class MyHttpClient {
  final Dio _dio = Dio(BaseOptions(
    baseUrl: Environment.baseUrl(),
    connectTimeout: _kDefaultConnectTimeout,
  ));

  Future<bool>? _updating;

  final VoidCallback? onRefreshFailure;

  MyHttpClient({this.onRefreshFailure}) {
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: _requestInterceptor,
        onError: _errorInterceptor,
      ),
    );
  }

  void _errorInterceptor(
    DioException err,
    ErrorInterceptorHandler handler,
  ) async {
    final resp = err.response;
    if (resp == null) {
      return handler.reject(err);
    }

    if (err is RefreshTokenError) {
      await Tokens.removeTokens();
      onRefreshFailure?.call();
    }

    handler.reject(err);
  }

  void _requestInterceptor(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    if (!Tokens.hasTokens) {
      await _headers(options, token: null);
      return handler.next(options);
    }
    final tokens = Tokens.tokens;

    assert(() {
      final now = DateTime.now().millisecondsSinceEpoch;
      final exp = Jwt.getExpiryDate(tokens.accessToken)!.millisecondsSinceEpoch;
      if (kDebugMode) {
        print('Token will expire in ${(exp - now) ~/ 1000} seconds');
      }
      return true;
    }());

    if (!Jwt.isExpired(tokens.accessToken)) {
      await _headers(options, token: tokens.accessToken);
    } else {
      //this is only executed when token != null and expired

      bool? result;
      if (_updating == null) {
        _updating = _refreshToken(tokens.accessToken, tokens.refreshToken);
        result = await _updating!;
        _updating = null;
      }

      if (result ?? await _updating!) {
        await _headers(options, token: Tokens.tokens.accessToken);
      } else {
        return handler.reject(
          RefreshTokenError(
            requestOptions: options,
            response: Response(
              requestOptions: options,
              data: null,
              statusCode: -1,
            ),
          ),
          true,
        );
      }
    }

    handler.next(options);
  }

  Future<void> _headers(RequestOptions options, {String? token}) async {
    if (token != null) {
      options.headers['Authorization'] = 'Bearer $token';
    }
    options.headers['Content-type'] = 'application/json';
    options.headers['Accept'] = 'application/json';
    options.headers['Accept-Charset'] = 'utf-8';
  }

  Future<bool> _refreshToken(
    String accessToken,
    String refreshToken,
  ) async {
    try {
      final res = await Dio(BaseOptions(
        baseUrl: Environment.baseUrl(),
      )).post(
        '/auth/refresh',
        options: Options(headers: {
          'content-type': 'application/json',
          'accept': 'application/json',
          // 'Authorization': 'Bearer $accessToken',
        }),
        data: {
          'refreshToken': refreshToken,
        },
      );

      if (res.successful) {
        await Tokens.setTokens(TokenPair(
          accessToken: res.data['accessToken'],
          refreshToken: res.data['refreshToken'],
        ));
        return true;
      }

      return false;
    } catch (_) {
      return false;
    }
  }

  Future<Response<T>> get<T>(
    String relativePath, {
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.get(
      relativePath,
      queryParameters: queryParameters,
    );
  }

  Future<Response<T>> post<T>(
    String relativePath, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.post(
      relativePath,
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> put<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.put(
      relativePath,
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> delete<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.delete(
      relativePath,
      queryParameters: queryParameters,
      data: data,
    );
  }

  Future<Response<T>> patch<T>(
    String relativePath, {
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    return _dio.patch(
      relativePath,
      queryParameters: queryParameters,
      data: data,
    );
  }
}

class RefreshTokenError extends DioException {
  RefreshTokenError({
    required super.requestOptions,
    super.response,
  });
}

extension StatusCodeX on Response {
  bool get successful {
    final sc = statusCode;
    if (sc == null) return false;
    return sc >= 200 && sc < 300;
  }
}
