const kShopId = 'ef9e19b4-1039-439b-940c-bc4dd0ca217b';

const kBorderRadiusSmall = 8.0;
const kBorderRadiusMedium = 16.0;
const kBorderRadiusBig = 20.0;

const kTabBarHeight = 72.0;

// this should be Figma step unit for this proj
const kStep = 8.0;

const kTextFieldHeight = 48.0;

const kRubUnicode = '₽';
const kDotUnicode = '\u2022';
