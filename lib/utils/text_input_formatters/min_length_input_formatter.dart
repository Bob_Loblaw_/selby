import 'package:flutter/services.dart';

class MinLengthInputFormatter extends TextInputFormatter {
  final int minLength;

  MinLengthInputFormatter(this.minLength);

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (newValue.text.length < minLength) {
      return oldValue;
    } else {
      return newValue;
    }
  }
}
