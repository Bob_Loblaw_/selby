import 'package:flutter/services.dart';
import 'package:selby/utils/utils.dart';

class NumRangeInputFormatter extends TextInputFormatter {
  final num? min;
  final num? max;

  NumRangeInputFormatter({
    required this.min,
    required this.max,
  });

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    final parsed = newValue.text.parseToNum();
    if (min != null && parsed < min!) {
      final s = min!.toCurrencyString();
      return TextEditingValue(
        text: s,
        selection: TextSelection.collapsed(offset: s.length),
      );
    }

    if (max != null && parsed > max!) {
      final s = max!.toCurrencyString();
      return TextEditingValue(
        text: s,
        selection: TextSelection.collapsed(offset: s.length),
      );
    }

    return newValue;
  }
}
