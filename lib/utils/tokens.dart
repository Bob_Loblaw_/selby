import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';

import 'crypto_storage.dart';

abstract class Tokens {
  static bool get hasTokens => _tokens.value != null;

  static final _tokens = ValueNotifier<TokenPair?>(null);
  static TokenPair get tokens => _tokens.value!;

  static void initTokens() {
    final at = Storage.get('accessToken');
    final rt = Storage.get('refreshToken');
    final has = at != null && rt != null;
    if (has) {
      _tokens.value = TokenPair(accessToken: at, refreshToken: rt);
    }
  }

  static Future<void> setTokens(TokenPair tokens) async {
    await Future.wait([
      Storage.put('accessToken', tokens.accessToken),
      Storage.put('refreshToken', tokens.refreshToken),
    ]);

    _tokens.value = tokens;
  }

  static Future<void> removeTokens() async {
    await Future.wait([
      Storage.remove('accessToken'),
      Storage.remove('refreshToken'),
    ]);

    _tokens.value = null;
  }
}

class TokenPair {
  final String accessToken;
  final String refreshToken;

  late final String _myUserId = Jwt.parseJwt(accessToken)['sub'];

  String get myUserId {
    return _myUserId;
  }

  TokenPair({
    required this.accessToken,
    required this.refreshToken,
  });

  @override
  String toString() {
    return 'TokenPair(\n'
        '\taccessToken: $accessToken,\n'
        '\trefreshToken: $refreshToken,\n)';
  }
}

mixin TokenStateAwareStateMixin<T extends StatefulWidget> on State<T> {
  void onNewTokenPair(TokenPair? pair);

  void _listener() {
    onNewTokenPair(Tokens._tokens.value);
  }

  @override
  void initState() {
    Tokens._tokens.addListener(_listener);
    super.initState();
  }

  @override
  void dispose() {
    Tokens._tokens.removeListener(_listener);
    super.dispose();
  }
}

class TokenStateObserver {
  final void Function(TokenPair?) onNewTokenPair;

  void _listener() {
    onNewTokenPair(Tokens._tokens.value);
  }

  TokenStateObserver({required this.onNewTokenPair}) {
    Tokens._tokens.addListener(_listener);
  }

  void dispose() {
    Tokens._tokens.removeListener(_listener);
  }
}
