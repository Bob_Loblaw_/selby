import 'package:flutter/material.dart';

abstract class TextStyles {
  static const h1 = TextStyle(
    fontSize: 28,
    height: 1,
    fontWeight: FontWeight.w600,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const h2 = TextStyle(
    fontSize: 18,
    height: 1.333,
    fontWeight: FontWeight.w600,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const small = TextStyle(
    fontSize: 16,
    height: 1.333,
    fontWeight: FontWeight.w400,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const smallSemibold = TextStyle(
    fontSize: 16,
    height: 1.333,
    fontWeight: FontWeight.w600,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const normal = TextStyle(
    fontSize: 18,
    height: 1.333,
    fontWeight: FontWeight.w400,
    leadingDistribution: TextLeadingDistribution.even,
  );

  static const normalSemibold = TextStyle(
    fontSize: 18,
    height: 1.333,
    fontWeight: FontWeight.w600,
    leadingDistribution: TextLeadingDistribution.even,
  );
}
