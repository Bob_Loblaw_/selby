part of 'app_theme.dart';

const ColorScheme _colorSchemeBright = ColorScheme(
  brightness: Brightness.light,
  // главные кнопки приложения
  primary: Color.fromARGB(255, 255, 194, 102),
  // чуть менее яркий, чем primary
  primaryContainer: Color.fromARGB(255, 255, 243, 224),
  // текст на primaryContainer
  onPrimaryContainer: Color.fromARGB(255, 25, 27, 48),
  // текст на главных кнопках
  onPrimary: Color.fromARGB(255, 25, 27, 48),
  //второстепенные элементы управления
  secondary: Color.fromARGB(255, 243, 244, 245),
  // текст на второстепенных элементы управления
  onSecondary: Color.fromARGB(255, 174, 177, 181),
  // цвет ошибки
  error: Color.fromARGB(255, 227, 148, 114),
  // цвет текста ошибки
  onError: Color.fromARGB(255, 255, 255, 255),
  // бэкграунд страницы
  background: Color.fromARGB(255, 255, 255, 255),

  // текст на бэкграунде
  onBackground: Color.fromARGB(255, 65, 77, 82),
  // подложка плитки
  surface: Color.fromARGB(255, 243, 244, 245),
  // акцентный бэкграунд
  surfaceVariant: Color.fromARGB(255, 255, 243, 224),
  // текст на плитке
  onSurface: Color.fromARGB(255, 65, 77, 82),
);
