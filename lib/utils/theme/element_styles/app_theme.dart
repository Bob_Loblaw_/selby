import 'package:selby/utils/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:selby/utils/utils.dart';

part 'button_styles.dart';
part 'color_schemes.dart';

ThemeData getAppTheme() {
  return ThemeData(
    colorScheme: _colorSchemeBright,
    iconButtonTheme: IconButtonThemeData(
      style: ButtonStyle(
        fixedSize: MaterialStateProperty.all(const Size(32.0, 32.0)),
        padding: MaterialStateProperty.all(const EdgeInsets.all(2.0)),
        shape: MaterialStateProperty.all(const OvalBorder(eccentricity: 0.0)),
      ),
    ),
    filledButtonTheme: FilledButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(
            vertical: 0.0,
            horizontal: 24.0,
          ),
        ),
        textStyle: MaterialStateProperty.all(
          TextStyles.normal,
        ),
      ),
    ),
    useMaterial3: true,
  );
}
