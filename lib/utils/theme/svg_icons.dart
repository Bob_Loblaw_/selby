abstract class SvgIcons {
  static const google = 'assets/svg/icon_google.svg';
  static const apple = 'assets/svg/icon_apple.svg';

  static const tinyArrow = 'assets/svg/icon_tiny_arrow.svg';
  static const chevronDown = 'assets/svg/icon_chevron_down.svg';

  static const home = 'assets/svg/icon_home.svg';
  static const heart = 'assets/svg/icon_heart.svg';
  static const shoppingCart = 'assets/svg/icon_shopping_cart.svg';
  static const shoppingBag = 'assets/svg/icon_shopping_bag.svg';
  static const profile = 'assets/svg/icon_profile.svg';

  static const shopLogo = 'assets/svg/icon_shop_logo.svg';

  static const search = 'assets/svg/icon_search.svg';
  static const filter = 'assets/svg/icon_filter.svg';
  static const back = 'assets/svg/icon_back.svg';

  static const minus = 'assets/svg/icon_minus.svg';
  static const plus = 'assets/svg/icon_plus.svg';
}
