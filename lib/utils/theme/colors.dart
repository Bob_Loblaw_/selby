import 'package:flutter/material.dart';

abstract class ThemeColorsBright {
  static const appColor = Colors.orange;
}

abstract class ThemeColorsDark {
  static const appColor = Colors.deepOrange;
}
