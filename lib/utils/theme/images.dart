abstract class Images {
  static const String shopCover = 'assets/image/shop_cover.jpg';

  static const String exampleProduct1 = 'assets/image/example_product_1.png';
  static const String exampleProduct2 = 'assets/image/example_product_2.png';
  static const String exampleProduct3 = 'assets/image/example_product_3.png';
  static const String exampleProduct4 = 'assets/image/example_product_4.png';
  static const String exampleProduct5 = 'assets/image/example_product_5.png';
  static const String exampleCategory1 = 'assets/image/example_cat_1.png';
  static const String exampleCategory2 = 'assets/image/example_cat_2.png';
  static const String exampleCategory3 = 'assets/image/example_cat_3.png';
  static const String exampleCategory4 = 'assets/image/example_cat_4.png';
  static const String exampleCategory5 = 'assets/image/example_cat_5.png';
  static const String exampleCategory6 = 'assets/image/example_cat_6.png';

  static const String noImagePlaceholder =
      'assets/image/no_image_placeholder.png';
}
