/// Stolen from Flutter and fixed a visual bug rather than doping workarounds

import 'dart:ui' show lerpDouble;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

@immutable
class FixedLinearBorderEdge {
  const FixedLinearBorderEdge();
}

/// An [OutlinedBorder] like [BoxBorder] that allows one to define a rectangular (box) border
/// in terms of zero to four [LinearBorderEdge]s, each of which is rendered as a single line.
///
/// The color and width of each line are defined by [side]. When [FixedLinearBorder] is used
/// with a class whose border sides and shape are defined by a [ButtonStyle], then a non-null
/// [ButtonStyle.side] will override the one specified here. For example the [FixedLinearBorder]
/// in the [TextButton] example below adds a red underline to the button. This is because
/// TextButton's `side` parameter overrides the `side` property of its [ButtonStyle.shape].
///
/// ```dart
///  TextButton(
///    style: TextButton.styleFrom(
///      side: const BorderSide(color: Colors.red),
///      shape: const LinearBorder(
///        side: BorderSide(color: Colors.blue),
///        bottom: LinearBorderEdge(),
///      ),
///    ),
///    onPressed: () { },
///    child: const Text('Red LinearBorder'),
///  )
///```
///
/// This class resolves itself against the current [TextDirection] (see [Directionality]).
/// Start and end values resolve to left and right for [TextDirection.ltr] and to
/// right and left for [TextDirection.rtl].
///
/// Convenience constructors are included for the common case where just one edge is specified:
/// [FixedLinearBorder.start], [FixedLinearBorder.end], [FixedLinearBorder.top], [FixedLinearBorder.bottom].
class FixedLinearBorder extends OutlinedBorder {
  /// Creates a rectangular box border that's rendered as zero to four lines.
  const FixedLinearBorder({
    super.side,
    this.start,
    this.end,
    this.top,
    this.bottom,
  });

  /// Creates a rectangular box border with an edge on the left for [TextDirection.ltr]
  /// or on the right for [TextDirection.rtl].
  FixedLinearBorder.start(
      {super.side, double alignment = 0.0, double size = 1.0})
      : start = FixedLinearBorderEdge(),
        end = null,
        top = null,
        bottom = null;

  /// Creates a rectangular box border with an edge on the right for [TextDirection.ltr]
  /// or on the left for [TextDirection.rtl].
  FixedLinearBorder.end({super.side, double alignment = 0.0, double size = 1.0})
      : start = null,
        end = FixedLinearBorderEdge(),
        top = null,
        bottom = null;

  /// Creates a rectangular box border with an edge on the top.
  FixedLinearBorder.top({super.side, double alignment = 0.0, double size = 1.0})
      : start = null,
        end = null,
        top = FixedLinearBorderEdge(),
        bottom = null;

  /// Creates a rectangular box border with an edge on the bottom.
  FixedLinearBorder.bottom(
      {super.side, double alignment = 0.0, double size = 1.0})
      : start = null,
        end = null,
        top = null,
        bottom = FixedLinearBorderEdge();

  /// No border.
  static const FixedLinearBorder none = FixedLinearBorder();

  /// Defines the left edge for [TextDirection.ltr] or the right
  /// for [TextDirection.rtl].
  final FixedLinearBorderEdge? start;

  /// Defines the right edge for [TextDirection.ltr] or the left
  /// for [TextDirection.rtl].
  final FixedLinearBorderEdge? end;

  /// Defines the top edge.
  final FixedLinearBorderEdge? top;

  /// Defines the bottom edge.
  final FixedLinearBorderEdge? bottom;

  @override
  FixedLinearBorder scale(double t) {
    return FixedLinearBorder(
      side: side.scale(t),
    );
  }

  /// Returns a copy of this LinearBorder with the given fields replaced with
  /// the new values.
  @override
  FixedLinearBorder copyWith({
    BorderSide? side,
    FixedLinearBorderEdge? start,
    FixedLinearBorderEdge? end,
    FixedLinearBorderEdge? top,
    FixedLinearBorderEdge? bottom,
  }) {
    return FixedLinearBorder(
      side: side ?? this.side,
      start: start ?? this.start,
      end: end ?? this.end,
      top: top ?? this.top,
      bottom: bottom ?? this.bottom,
    );
  }

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    final Rect adjustedRect =
        dimensions.resolve(textDirection).deflateRect(rect);
    return Path()..addRect(adjustedRect);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    return Path()..addRect(rect);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {
    final EdgeInsets insets = dimensions.resolve(textDirection);
    final bool rtl = textDirection == TextDirection.rtl;

    final Path path = Path();
    final Paint paint = Paint()..strokeWidth = 0.0;

    void drawEdge(Rect rect, Color color) {
      paint.color = color;
      path.reset();
      path.moveTo(rect.left, rect.top);
      if (rect.width == 0.0) {
        paint.style = PaintingStyle.stroke;
        path.lineTo(rect.left, rect.bottom);
      } else if (rect.height == 0.0) {
        paint.style = PaintingStyle.stroke;
        path.lineTo(rect.right, rect.top);
      } else {
        paint.style = PaintingStyle.fill;
        path.lineTo(rect.right, rect.top);
        path.lineTo(rect.right, rect.bottom);
        path.lineTo(rect.left, rect.bottom);
      }
      canvas.drawPath(path, paint);
    }

    if (start != null && side.style != BorderStyle.none) {
      final Rect insetRect = Rect.fromLTWH(
        rect.left,
        rect.top + insets.top,
        rect.width,
        rect.height - insets.vertical,
      );
      final double x = rtl ? rect.right - insets.right : rect.left;
      final double width = rtl ? insets.right : insets.left;
      final double height = insetRect.height;
      final double y = insetRect.top;
      final Rect r = Rect.fromLTWH(x, y, width, height);
      drawEdge(r, side.color);
    }

    if (end != null && side.style != BorderStyle.none) {
      final Rect insetRect = Rect.fromLTWH(
        rect.left,
        rect.top + insets.top,
        rect.width,
        rect.height - insets.vertical,
      );
      final double x = rtl ? rect.left : rect.right - insets.right;
      final double width = rtl ? insets.left : insets.right;
      final double height = insetRect.height;
      final double y = insetRect.top;
      final Rect r = Rect.fromLTWH(x, y, width, height);
      drawEdge(r, side.color);
    }

    if (top != null && side.style != BorderStyle.none) {
      final double width = rect.width;
      final double startX = rect.left;
      final double x = rtl ? rect.width - startX - width : startX;
      final Rect r = Rect.fromLTWH(x, rect.top, width, insets.top);
      drawEdge(r, side.color);
    }

    if (bottom != null && side.style != BorderStyle.none) {
      final double width = rect.width;
      final double startX = rect.left;
      final double x = rtl ? rect.width - startX - width : startX;
      final Rect r = Rect.fromLTWH(
        x,
        rect.bottom - insets.bottom,
        width,
        side.width,
      );
      drawEdge(r, side.color);
    }
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is FixedLinearBorder &&
        other.side == side &&
        other.start == start &&
        other.end == end &&
        other.top == top &&
        other.bottom == bottom;
  }

  @override
  int get hashCode => Object.hash(side, start, end, top, bottom);

  @override
  String toString() {
    if (this == FixedLinearBorder.none) {
      return 'LinearBorder.none';
    }

    final StringBuffer s =
        StringBuffer('${objectRuntimeType(this, 'LinearBorder')}(side: $side');

    if (start != null) {
      s.write(', start: $start');
    }
    if (end != null) {
      s.write(', end: $end');
    }
    if (top != null) {
      s.write(', top: $top');
    }
    if (bottom != null) {
      s.write(', bottom: $bottom');
    }
    s.write(')');
    return s.toString();
  }
}
