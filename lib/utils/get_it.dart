import 'package:get_it/get_it.dart';
import 'package:selby/shared/api/api.dart';

import 'utils.dart';

final getIt = GetIt.instance;

void getItSetup() {
  // http client dep
  getIt.registerSingleton<MyHttpClient>(MyHttpClient());

  // shared
  getIt.registerSingleton<CategoriesApi>(const CategoriesApiMock());
  getIt.registerSingleton<CategoryListingApi>(const CategoryListingApiMock());
}
