import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class DimensionMeasurer extends StatefulWidget {
  final Widget Function(
    BuildContext context,
    Widget child,
    CalculatedChildDimension dim,
  ) onMeasured;
  final Widget child;

  const DimensionMeasurer({
    Key? key,
    required this.onMeasured,
    required this.child,
  }) : super(key: key);

  @override
  State<DimensionMeasurer> createState() => _DimensionMeasurerState();
}

class _DimensionMeasurerState extends State<DimensionMeasurer> {
  final globalKey = GlobalKey();
  CalculatedChildDimension? dim;

  void callback() {
    dim = null;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final ctx = globalKey.currentContext;
      assert(ctx != null);
      final ro = ctx!.findRenderObject();
      assert(ro != null);
      if (ro is RenderSliver) {
        final g = ro.geometry;
        assert(g != null);
        dim = CalculatedSliverDimension(g!.scrollExtent);
      } else {
        ro as RenderBox;
        final size = ro.size;
        dim = CalculatedRenderBoxDimension(size);
      }

      setState(() {});
    });
  }

  @override
  void initState() {
    callback();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant DimensionMeasurer oldWidget) {
    if (widget.child.key != oldWidget.child.key ||
        widget.child.runtimeType != oldWidget.child.runtimeType) {
      callback();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (dim == null) {
      return KeyedSubtree(
        key: globalKey,
        child: widget.child,
      );
    } else {
      return widget.onMeasured(context, widget.child, dim!);
    }
  }
}

abstract class CalculatedChildDimension {
  CalculatedChildDimension();
}

class CalculatedSliverDimension extends CalculatedChildDimension {
  final double mainAxisExtent;

  CalculatedSliverDimension(this.mainAxisExtent);
}

class CalculatedRenderBoxDimension extends CalculatedChildDimension {
  final Size size;

  CalculatedRenderBoxDimension(this.size);
}
