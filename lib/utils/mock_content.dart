import 'package:selby/common/models/models.dart';
import 'package:selby/utils/utils.dart';

final attributes = <EavValue>[
  EavValue.number(
    id: '123',
    key: 'Ширина, см',
    value: 60,
  ),
  EavValue.number(
    id: '123',
    key: 'Глубина, см',
    value: 70,
  ),
  EavValue.number(
    id: '123',
    key: 'Высота, см',
    value: 86,
  ),
  EavValue.number(
    id: '123',
    key: 'Вес товара, г',
    value: 12000,
  ),
  //
  //
  EavValue.string(
    id: '123',
    key: 'Материал обивки',
    value: 'Велюр искусственный',
  ),
  EavValue.string(
    id: '123',
    key: 'Материал корпуса',
    value: 'Фанера',
  ),
  EavValue.string(
    id: '123',
    key: 'Покрытие корпуса',
    value: 'Матовое, водостойкое',
  ),
  //
  //
  EavValue.number(
    id: '123',
    key: 'Высота сиденья, см',
    value: 40,
  ),
  EavValue.number(
    id: '123',
    key: 'Ширина сиденья, см',
    value: 50,
  ),
  EavValue.number(
    id: '123',
    key: 'Глубина сиденья, см',
    value: 50,
  ),
  EavValue.number(
    id: '123',
    key: 'Высота спинки, см',
    value: 86,
  ),
  EavValue.number(
    id: '123',
    key: 'Ширина спинки, см',
    value: 50,
  ),
  //
  //
  EavValue.string(
    id: '123',
    key: 'Тип',
    value: 'Кресло',
  ),
  EavValue.string(
    id: '123',
    key: 'Форма поставки',
    value: 'В разобранном виде',
  ),
  EavValue.string(
    id: '123',
    key: 'Цвет',
    value: 'Бежевый, серый',
  ),
  EavValue.string(
    id: '123',
    key: 'Вид выпуска товара',
    value: 'Фабричное производство',
  ),
  EavValue.string(
    id: '123',
    key: 'Гарантийный срок',
    value: '12 месяцев',
  ),
  EavValue.string(
    id: '123',
    key: 'Страна-изготовитель',
    value: 'Россия',
  ),
];

final products = [
  Product(
    id: '123',
    label: 'Напольная лампа стеклянная Astoria в гостинную',
    descriptionShort: '',
    coverId: null,
    price: 4800,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Кресло SÉMÉON',
    descriptionShort: 'Глубокое, удобное и компактное кресло Séméon с '
        'красивыми веретенообразными ножками покорит любителей интерьеров '
        'в стиле арт-деко и просто поклонников красивой мебели. '
        'Оно наполнит Вашу гостиную пылкой чувственной атмосферой.',
    coverId: Images.exampleProduct5,
    price: 12000,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label:
        'Кресло Софа с деревянными ножками и велюровой подушкой цветом изумруда',
    descriptionShort: '',
    coverId: Images.exampleProduct5,
    price: 12000,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Мягкий диван с выдвижной столешницей',
    descriptionShort: '',
    coverId: Images.exampleProduct5,
    price: 12000,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Настольная лампа стеклянная Ator '
        'в современном дизайне в кооперации с '
        'Арт-Студией Артемия Лебедева (Ограниченное издание)',
    descriptionShort: null,
    coverId: Images.exampleProduct5,
    price: 12000,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Lorem ipsum',
    descriptionShort: '',
    coverId: Images.exampleProduct2,
    price: 18000,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Шкаф купе деревянный FortBoyard',
    descriptionShort: '',
    coverId: Images.exampleProduct3,
    price: 8800,
    attributes: attributes,
  ),
  Product(
    id: '123',
    label: 'Барный стул с ножками из дерева',
    descriptionShort: '',
    coverId: Images.exampleProduct4,
    price: 6600,
    attributes: attributes,
  ),
];

const categories = <Category>[
  Category(
    id: '123',
    title: 'Новые поступления',
    avatarId: Images.exampleCategory1,
  ),
  Category(
    id: '123',
    title: 'Ванная',
    avatarId: Images.exampleCategory2,
  ),
  Category(
    id: '123',
    title: 'Спальня',
    avatarId: Images.exampleCategory3,
  ),
  Category(
    id: '123',
    title: 'Раковина',
    avatarId: null,
  ),
  Category(
    id: '123',
    title: 'Хранение',
    avatarId: null,
  ),
  Category(
    id: '123',
    title: 'Унитаз',
    avatarId: null,
  ),
  Category(
    id: '123',
    title: 'Смеситель',
    avatarId: null,
  ),
  Category(
    id: '123',
    title: 'Аксессуары',
    avatarId: null,
  ),
];

List<Product> cycleProducts(int n) {
  assert(n > 0);
  final l = products.length;
  return List<Product>.generate(
    n,
    (i) => products[i % l],
  );
}

List<Category> cycleCategories(int n) {
  assert(n > 0);
  final l = categories.length;
  return List<Category>.generate(
    n,
    (i) => categories[i % l],
  );
}
