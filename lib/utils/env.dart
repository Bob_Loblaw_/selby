import 'package:flutter/foundation.dart';

abstract class Environment {
  static const _prodUrl = String.fromEnvironment(
    'PROD_URL',
    defaultValue: 'https://store.selby.co',
  );

  static const _devUrl = String.fromEnvironment(
    'DEV_URL',
    defaultValue: 'https://api.mp.alt-point.com',
  );

  static String baseUrl() {
    switch (true) {
      case kDebugMode || kProfileMode:
        {
          return _devUrl;
        }
      default:
        {
          return _prodUrl;
        }
    }
  }
}
