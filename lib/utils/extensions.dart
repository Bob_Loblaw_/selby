extension Formatting on num {
  String toCurrencyString({
    String? suffix,
    String decimalSeparator = ',',
  }) {
    String recursive(num num, bool pad) {
      if (num < 1000.0) {
        if (num is int) {
          if (pad) {
            return num.toString().padLeft(3, '0');
          } else {
            return num.toString();
          }
        }

        final fixed = num.toStringAsFixed(1);
        final whole = fixed.substring(0, fixed.length - 2);
        if (fixed.endsWith('.0')) {
          if (pad) {
            return whole.padLeft(3, '0');
          } else {
            return whole;
          }
        } else {
          final rest = fixed
              .substring(fixed.length - 2, fixed.length)
              .replaceAll('.', decimalSeparator);
          if (pad) {
            return '${whole.padLeft(3, '0')}$rest';
          } else {
            return '$whole$rest';
          }
        }
      }

      final left = num ~/ 1000;
      final right = num % 1000;
      return '${recursive(left, false)} ${recursive(right, true)}';
    }

    final rec = recursive(this, false);
    if (suffix == null) return rec;
    return '$rec $suffix';
  }
}

extension Parsing on String {
  num parseToNum() {
    return num.parse(replaceAll(',', '').replaceAll(' ', ''));
  }
}

extension IndexedMapping<E> on List<E> {
  Iterable<T> mapIndexed<T>(T Function(int, E) fn) {
    int i = 0;
    return map((e) => fn(i++, e));
  }
}
